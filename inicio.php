<?php
session_start();

$nombre = $_SESSION['nombre'];
$id_user = $_SESSION['id'];



if ($nombre == '' || $nombre == null) {
    echo "<script>
    alert('¡Atención! sesión no iniciada, se redireccionará para su inicio de sesión.');
    window.location = '../login.php';
    </script>";
    die();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/inicio.css">
    <title>Inicio</title>
</head>

<body>


    <div id="containerx">
        <header id="headerx">
            <div id="menu">
                <ul>
                    <li id="logo">
                        <a href="#">
                            <img src="RES/img/1-logo.png" width="80" />
                        </a>
                    </li>
                    <li id="perfil">
                        <a href="javascript:abrir()">
                            Perfil
                        </a>
                    </li>

                    <li id="encuesta">
                        <a href="./inicio/encuesta.php" id="encuestaNoRealizada">
                            Encuesta
                        </a>
                        <a href="#" id="encuestaRealizada" style="display: none;" onclick="StateOfQuiz()">
                            Encuesta
                        </a>
                    </li>

                    <li id="cerrarSesion">
                        <a href="./CerrarSesion.php">
                            Cerrar sesión
                        </a>
                    </li>
                </ul>
            </div>
        </header>
    </div>

    <div id="container1">
        <main>
            <div class="text">

                <h1 id="user">Bienvenido!, usuario</h1>
            </div>
        </main>

        <br>

        <div id="actividades" class="seccion">

            <div class="subseccion subseccion1 paralelogramo">
                <h3>Actividades</h3>
            </div>


            <div class="subseccion subseccion2 paralelogramo">

                <div id="#" class="btn">Registrar nueva actividad</div>
                <div id="#" class="btn">Editar o eliminar actividades</div>

            </div>

            <div id="subimagen1" class="subimagen subseccion subseccion2 paralelogramo">

            </div>


        </div>

        <br><br>

        <div id="itinerarios" class="seccion">

            <div class="subseccion subseccion1 paralelogramo">
                <h3>Itinerarios</h3>
            </div>
            <div class="subseccion subseccion2 paralelogramo">
                <div id="#" class="btn">Visualizar mi itinerario</div>
                <div id="#" class="btn">Crear nuevo itinerario</div>
                <div id="#" class="btn">Editar o eliminar itinerarios</div>
            </div>

            <div id="subimagen2" class=" subimagen subseccion subseccion2 paralelogramo">

            </div>
        </div>

    </div>

    <footer> Autor: Juan Arias Castillo ITSM - MSC - Tecnologías de programación</footer>

    <?php
    echo "<script> var x = document.getElementById('user');
            x.textContent='Bienvenido!, ' +'${nombre}';
            </script>";

    session_start();

    include("conexion.php");

    $query2 = mysqli_query($conn, "SELECT * FROM encuesta WHERE id_user = '$id_user'");
    $res = mysqli_num_rows($query2);


    if ($res >= 1) {
        $_SESSION['encuesta'] = 'S';
        echo "<script>

    document.getElementById('encuestaNoRealizada').style.display = 'none';
    document.getElementById('encuestaRealizada').style.display = 'block';

    </script>";
    } else {
        $_SESSIONÑ['encuesta'] = 'N';
        echo "<script>

    document.getElementById('encuestaNoRealizada').style.display = 'block';
    document.getElementById('encuestaRealizada').style.display = 'none';
    </script>";
    }
    ?>

    <script>
        function StateOfQuiz() {
            alert('Ya ha respondido esta encuesta.');
        }
    </script>

</body>

</html>