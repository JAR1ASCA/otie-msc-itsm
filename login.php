<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/f5f0dc57eb.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="CSS/estiloLoginRegister.css">
    <title>Iniciar Sesión</title>
</head>

<body>

    <!-- Algoritmos de planificación -->

    <form class="formulario" action="" method="post">

        <h1>Iniciar Sesión</h1>

        <center>
            <div id="statusLogin" style="
            background-color: rgb(3, 180, 3); height: 30px; width:95%; display:flex;
            justify-content: center; align-items: center; display:none;">

                Registro exitoso

            </div>
        </center>

        <div class="container">

            <div class="input-container">
                <i class="fas fa-user icon"></i>
                <input type="email" placeholder="Correo electrónico" name="correo" required autofocus>
            </div>

            <div class="input-container">
                <i class="fas fa-key icon"></i>
                <input type="password" placeholder="Contraseña" name="contraseña" required>
            </div>

            <div>
                <input type="submit" class="boton" value="Iniciar Sesión" name="btnLogin">
            </div>

            <p>Al continuar, acepta nuestra política de privacidad y términos de servicio.</p>

            <p>
                ¿Nuevo en OTI:E?
                <a class="link" href="register.php">Registrarse</a>
            </p>

        </div>

    </form>



    <?php
    include("conexion.php");
    
    //variables
    $contraseña = $_POST["contraseña"];
    $correo = $_POST["correo"];
    
    //login
    if (isset($_POST["btnLogin"])) {
    
        $resultado = mysqli_query($conn, "SELECT contraseña, nombre, id_usuario FROM usuario WHERE correo = '$correo'");
    
        if ($resultado) {
            while ($row = $resultado->fetch_array()) {
                $pwd = $row['contraseña'];
                $nombre = $row['nombre'];
                $id = $row['id_usuario'];
            }
        }
    
        if (password_verify($contraseña, $pwd)) {
            echo "<script> 
                    // alert('Bienvenido $nombre'); 
                    window.location = 'inicio.php';
                </script>";

            session_start();
            $_SESSION['nombre'] = $nombre;
            $_SESSION['id'] = $id;

    
        } else {
            // echo "<script> alert('¡Error! Verifique los datos ingresados');</script>";
            echo "<script> var x = document.getElementById('statusLogin');
            x.style.display = 'flex';
            x.style.backgroundColor = 'red';
            x.style.color = 'white';
            x.textContent = 'Error: Verifique sus datos.';
            </script>";
        }
    
    
        // $query = mysqli_query($conn, "SELECT * FROM usuario WHERE correo = '$correo' AND contraseña = '$contraseña'");
        // $nb = mysqli_num_rows($query);
    
        // if ($nb == 1) {
        //     echo "<script> alert('Bienvenido $nombre'); window.location =  'principal.html'</script>";
        // } else {
        //     echo "<script> alert('¡Error! Verifique los datos ingresados'); window.location =  'login.html'</script>";
        // }
    }
    ?>

</body>

</html>

