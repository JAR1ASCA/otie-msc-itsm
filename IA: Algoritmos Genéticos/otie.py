# -*- coding: utf-8 -*-
"""otie.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1epiyhrThOlNCROS58Dt209_KpZn446PH
"""

pip install pymysql

def decimal_a_binario(numero_decimal):
    numero_binario = 0
    multiplicador = 1

    while numero_decimal > 0:
        # se almacena el módulo en el orden correcto
        # print("El numero es = ", numero_decimal)
        numero_binario = numero_binario + numero_decimal % 2 * multiplicador
        numero_decimal //= 2
        multiplicador *= 10

    # print("El binario es = ", numero_binario)

    return numero_binario

# def binario_a_decimal(numero_binario):
#   numero_decimal = 0
  
#   # print(numero_binario)


#   for posicion, digito_string in enumerate(numero_binario[::-1]):
#     numero_decimal += int(digito_string) * 2 ** posicion

#   return numero_decimal


def binario_a_decimal(cromosoma):
  # print("\ncromosoma = ",cromosoma,"\n")


  #Invertir el cromosoma
  binario = cromosoma[::-1]
  posicion = 0
  decimal = 0

  # print(binario)
  for i in binario:
    multiplicacion = 2**posicion
    decimal += int(i) * multiplicacion
    posicion += 1

  return decimal

# print(binario_a_decimal(px[0]))


def n_bits(index):
  if(index == 1):
    return 4
  elif(index == 2):
    return 6
  elif(index == 4):
    return 5
  elif(index == 5):
    return 7
  elif(index == 7):
    return 5
  elif(index == 8):
    return 6
  elif(index == 10):
    return 5
  elif(index == 11):
    return 6
  elif(index == 13):
    return 5
  elif(index == 14):
    return 9
  elif(index == 16):
    return 4
  elif(index == 17):
    return 9
  elif(index == 19):
    return 5
  elif(index == 20):
    return 8
  elif(index == 22):
    return 5
  elif(index == 23):
    return 8
  elif(index == 25):
    return 5
  elif(index == 26):
    return 10

def index_duracion_ok(index): #index de duración?
  if(index == 2):
    return True
  elif(index == 5):
    return True
  elif(index == 8):
    return True
  elif(index == 11):
    return True
  elif(index == 14):
    return True
  elif(index == 17):
    return True
  elif(index == 20):
    return True
  elif(index == 23):
    return True
  elif(index == 26):
    return True
  else:
    False

# def binario_a_decimal(numero_binario):
#   numero_decimal = 0
  
#   print(numero_binario)


#   for posicion, digito_string in enumerate(numero_binario[::-1]):
#     numero_decimal += int(digito_string) * 2 ** posicion

#   return numero_decimal

# bin = binario_a_decimal(px[0])

# print(bin)

import pymysql
import numpy as np
import random

conn = pymysql.connect( 
    host='bragzxhb9zcm7pt0s3nc-mysql.services.clever-cloud.com', 
    user= 'udvtsji24undktci', 
    passwd='I2uh1ZG0j7JSWl2YbE9T', 
    db='bragzxhb9zcm7pt0s3nc' )
cur = conn.cursor()
num = cur.execute( "SELECT Desayuno_HorarioFijo, Desayuno_Horario, Desayuno_Duracion,     Comida_HorarioFijo, Comida_Horario, Comida_Duracion,      Cena_HorarioFijo, Cena_Horario, Cena_Duracion,      Bañarse_HorarioFijo, Bañarse_Horario, Bañarse_Duracion,      HacerTarea_HorarioFijo, HacerTarea_Horario, HacerTarea_Duracion,      ClasesVirtuales_HorarioFijo, ClasesVirtuales_Horario, ClasesVirtuales_Duracion,      VerTvSerie_HorarioFijo, VerTvSerie_Horario, VerTvSerie_Duracion,      AvanceProyecto_HorarioFijo, AvanceProyecto_Horario, AvanceProyecto_Duracion,      Dormir_HorarioFijo, Dormir_Horario, Dormir_Duracion  FROM encuesta2" )

print("Registros = ", num,"\n")
val = cur.fetchall()

lista = []

for x in val:
  lista += [x]

# print(lista)

array = np.array(lista)

print(array)

print("\narrary = ",array.shape)

conn.close()

print("Viejo array = ",array[2,:])

"""Un cromosoma está conformado por un determinado numero de actividades, cada actividad con sus respectivos atributos: (horario fijo, horario, duracion)

Ejemplo:

[<font color='red'> 1</font> 
<font color='orange'>1010</font> 
<font color='green'>101011</font> 
<font color='yellow'>0</font> 
<font color='blue'>0</font> 
<font color='orangered'>101111</font> 
<font color='white'>1</font> 
<font color='cyan'>10110</font> 
<font color='tan'>101100</font> 
<font color='springgreen'>1</font> 
<font color='salmon'>101</font> 
<font color='red'>100110</font> 
<font color='yellow'>1</font> 
<font color='black'>10110</font> 
<font color='gray'>101000101</font> 
<font color='green'>0</font> 
<font color='orange'>0</font> 
<font color='pink'>0</font> 
<font color='white'>0</font> 
<font color='red'>0</font> 
<font color='blue'>0</font> 
<font color='tan'>0</font> 
<font color='orangered'>0</font> 
<font color='cyan'>101110</font>
<font color='gray'>1</font> 
<font color='green'>10101</font> 
<font color='yellow'>111100011</font>]

Es decir:

[<font color='red'> HorarioFijo</font> 
<font color='orange'>Horario</font> 
<font color='green'>Duración</font> 
<font color='yellow'>HorarioFijo</font> 
<font color='blue'>Horario</font> 
<font color='orangered'>Duración</font> 
<font color='white'>HorarioFijo</font> 
<font color='cyan'>Horario</font> 
<font color='tan'>Duración</font> 
<font color='springgreen'>HorarioFijo</font> 
<font color='salmon'>Horario</font> 
<font color='red'>Duración</font> 
<font color='yellow'>HorarioFijo</font> 
<font color='black'>Horario</font> 
<font color='gray'>Duración</font> 
<font color='green'>HorarioFijo</font> 
<font color='orange'>Horario</font> 
<font color='pink'>Duración</font> 
<font color='white'>HorarioFijo</font> 
<font color='red'>Horario</font> 
<font color='blue'>Duración</font> 
<font color='tan'>HorarioFijo</font> 
<font color='orangered'>Horario</font> 
<font color='cyan'>Duración</font>
<font color='gray'>HorarioFijo</font> 
<font color='green'>Horario</font> 
<font color='yellow'>Duración</font>]

Correspondientes a las actividades: Desayunar, Comer, Cenar, Bañarse, Hacer tarea, Clases virtuales, Ver tv o serie, Avanzar proyecto, Dormir

**Nota:** cada gen o atributo, tiene su propia longitud
"""

n_genes = array[0,:].size #numero de genes o caracteristicas
n_cromosomas = array[:,0].size #numero de cromosomas o individuos

print("#genes = ", n_genes)
print("#cromosomas = ", n_cromosomas)

#población inicial
for j in range(0,n_cromosomas):
  listax = []
  for i in range(0,n_genes):
    if array[j,i] == 'S':
      # array[0,i] = 1
      listax += [1]

    elif array[j,i] == 'N':
      # array[0,i] = 0
     listax += [0]      
  
    else:
      numDec = int(array[j,i])
      numBin = decimal_a_binario(numDec)
      strbin  = str(numBin)
      lenNumBin = len(strbin)
      limit = n_bits(i)

      if(lenNumBin <limit):
        falta = limit - lenNumBin
        # print("faltan = ",falta," len es = ", lenNumBin," y el limite es = ",limit)
        for x in range(falta):
          strbin = "0"+ strbin

        # print("longitud = ", lenNumBin, "numero binario = ",str(numBin))
      listax += [strbin]
      # print(listax)

  array2 = np.array(listax)
  # print("\narray2 ",array2)

  if(j == 0):
    px = np.array(listax)
  else:
    px = np.vstack([px,array2])


print("\npoblación Total:\n ",px.shape)
print("\npoblación Total:\n ",px[0,:]) #Poblacion total (no es parte del algoritmo genetico)

print("decimal = ",binario_a_decimal("0111100011"))

total = 43+ 47+ 44+ 38+ 325+ 46+ 483
print(total)

def fitness(individuo,max_horas = 16): #Devuelve el numero de horas

  duracion_total = 0;
  for indice, valor in enumerate(individuo):
    # print("indice = ", indice, " valor = ", valor)

    if index_duracion_ok(indice):
      duracion_total += binario_a_decimal((valor))
      
      
  duracionT_horas = duracion_total/60      
  # print("duracion = " ,duracion_total, " minutos")
  # print("duracion = " ,duracionT_horas, " horas")

  if(duracionT_horas<= max_horas):
    # print("duracion = " ,duracion_total, " minutos")
    # print("duracion = " ,duracionT_horas, " horas")
    return duracionT_horas
  else:
    # print("ERROR: el individuo supera el máximo numero de horas, fit = ", duracionT_horas )
    return 0

  


z = fitness(px[9,:])
print("fitness = ",z)

def pob_inicial(poblacion,max_horas=16):

  filas,columnas = poblacion.shape #px.shape #px = poblacion total
  horas_max = max_horas #16 #maximo de horas por día  
  list_z = []
  i = 0

  # print("filas = ", filas)

  # Creacion poblacion inicial p0
  for index in range(filas):
    z = fitness(px[index,:],horas_max)  

    print("\n fitness ===== Index [",index,"] valor px = ",px[index,:])
    print("z = ", z)
    
    if z>0:
      list_z += [z]
    
      if i == 0:
        p0 = np.array(px[index])
      else:
        p0 = np.vstack([p0,px[index]])
      i+=1


  z_fit = np.array(list_z)

  # print("Población inicial:\n", p0.shape)
  # print("Población inicial:\n", p0)
  # print("Z:\n", z_fit.shape)
  return p0
 
pob_inicial(px,16)

def fit_pob(poblacion): #devuelve el fit de la población
  filas,colum = poblacion.shape
  list_fitPob = []
  for i in range(filas):
    list_fitPob += [fitness(poblacion[i])]
  fitPob = np.array(list_fitPob)
  return fitPob

# fitPob = fit_pob(p0)
# print(fitPob.size)

def ruleta(poblacion): #devuelve un individuo al azar según su probabilidad
  # Fitness y probabilidad de la población
  # probaPob = np.zeros(len(poblacion))
  # fitPob = np.zeros(len(poblacion))

  # print(" ", len(poblacion))
  
  fitPob = np.copy(fit_pob(poblacion))
  total = sum(fitPob)

  list_probaPob = []
  for i in range(len(fitPob)):
    list_probaPob += [fitPob[i] / total]
  
  probaPob = np.array(list_probaPob)

  aleatorio = random.random()
  posicion = 0
  intervalo = probaPob[0]

  for i in range(len(probaPob)-1):
    if aleatorio < intervalo:
      posicion = i
      break
    else:
      intervalo += probaPob[i+1]
  
  return poblacion[posicion]

# p1 = ruleta(p0)
# print(p1)

def crossover(padre1,padre2):
  mascara = np.random.randint(0,2,size=(len(padre1)))

  # print('mascara = ',mascara)
  # print('\npadre1 = ',padre1)
  # print('\npadre2 = ',padre2)


  list_hijo = []
  for i in range(len(mascara)):
    if mascara[i] == 1:
      list_hijo += [padre1[i]]
    else:
      list_hijo += [padre2[i]]

  hijo = np.array(list_hijo)
  return hijo

# hijo = crossover(px[0],px[1])  
# print("\nhijo = ", hijo)

def mutacion (cromosoma,porcentaje): #muta un individuo o gen, dependiendo del porcentaje de posibilidad
  n_mutaciones = round((len(cromosoma)*porcentaje) / 100)
  #print('No. mutaciones = ',n_mutaciones)
  for i in range(n_mutaciones):
    posicion = np.random.randint(0,len(cromosoma))
    #print('posicion = ',posicion)
    if cromosoma[posicion] == 1:
      cromosoma[posicion] = 0
    else:
      cromosoma[posicion] = 1

  return cromosoma

"""Nota: modificar fitness"""

################################################################################
############################## Algoritmo genético ##############################
################################################################################
def AG():
  import numpy as np
  import random

  horas_max = 16 #maximo numero de horas por día para las acts. 16 por defecto
  n_gens = 100 #numero de generaciones
  p_mut = 1 #porcentaje de mutación
  
  #Poblacion inicial
  p0 = pob_inicial(px,horas_max)
  # newPob = np.zeros(p0.shape)  #list_newPob = []
  


  n_ind,colm = p0.shape #numero de individuos o cromosomas, - columnas

  print("cromosomas = ",n_ind) 
  # print(p0)
  

  for i in range(n_gens):
    #print('Generación = ',i+1)
    z = 0
    list_x = []
    newPob1 = np.array(list_x)

    for j in range(n_ind):
      list_newPob = []

      if i == 0:
        #Seleccion
        p1 = ruleta(p0)
        p2 = ruleta(p0)

        # print("\npadre 1, index [",j,"]:")
        # print(p1)
        # print("\npadre 2, index [",j,"]:")
        # print(p2)

        #Crossover
        hijo = crossover(p1,p2)
        # print("\nhijo, index [",j,"]:")
        # print(hijo)

        #Mutacion
        hijo_m = mutacion(hijo,p_mut)
        # print("\nhijo mutado, index [",j,"]:")
        # print(hijo_m)

        #Nueva generación
        list_newPob += [hijo_m]
        newPob = np.array(list_newPob)

        # print("\n\n")
      else:
        
        # while 1:
          #Seleccion
          p1 = ruleta(newPob)
          p2 = ruleta(newPob)
          #Crossover
          hijo = crossover(p1,p2)
          #Mutacion
          hijo_m = mutacion(hijo,p_mut)
          #Nueva generación
          list_newPob += [hijo_m]
          newPob1 = np.array(list_newPob)
          # print("newPob1 = ",newPob1)
          # newPob = np.vstack([newPob, list_newPob])


    if i >0:
      z = fitness(newPob1[0])
      print("fitness = ",z)
    if (z>0):
      newPob = np.vstack([newPob, newPob[0]])
    else:
      j -=1
    print('Generación #',i+1,' valor óptimo = ',newPob[0])




AG()

