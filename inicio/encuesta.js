function main() {
    //***** alimentos*****
    //preparar comida
    var preparaComidaSi = document.getElementById("preparaComidaSi").checked;
    const contenedorPreparaComida = document.getElementById("noPreparaAlimento");

    //***** limpieza*****
    //lavar trastes
    var lavaTrastesSi = document.getElementById("lavaTrastesSi").checked;
    const contenedorLavaTrastes = document.getElementById("noLavaTrastes");
    //barrer
    var barreSi = document.getElementById("barreSi").checked;
    const contenedorBarre = document.getElementById("noBarre");
    //trapear
    var trapeaSi = document.getElementById("trapeaSi").checked;
    const contenedorTrapea = document.getElementById("noTrapea");
    //lavar ropa
    var lavarRopaSi = document.getElementById("lavarRopaSi").checked;
    const contenedorLavarRopa = document.getElementById("noLavaRopa");

    //***** higiene personal*****
    //maquillarse
    var maquillarseSi = document.getElementById("maquillarseSi").checked;
    const contenedorMaquillarse = document.getElementById("noSeMaquilla");
    const contenedorDesmaquillarse = document.getElementById("noSeDesmaquilla");

    //***** estudiar*****
    //clases virtuales
    var clasesVirtualesSi = document.getElementById("clasesVirtualesSi").checked;
    const contenedorClasesVirtuales = document.getElementById("noClasesVirtuales");
    //clases presenciales
    var clasesPresencialesSi = document.getElementById("clasesPresencialesSi").checked;
    const contenedorClasesPresenciales = document.getElementById("noClasesPresenciales");
    const contenedorClasesPresenciales2 = document.getElementById("noClasesPresenciales2");
    //estudiar (fuera del horario de clases)
    var estudioAutodidactaSi = document.getElementById("estudioAutodidactaSi").checked;
    const contenedorEstudioAutodidacta = document.getElementById("noEstudioAutodidacta");
    //avance proyecto
    var avanceProyectoSi = document.getElementById("avanceProyectoSi").checked;
    const contenedorAvanceProyecto = document.getElementById("noAvanceProyecto");

    //***** entretenimiento*****
    //ver TV o serie
    var verTvSerieSi = document.getElementById("verTvSerieSi").checked;
    const contenedorVerTvSerie = document.getElementById("noVerTvSerie");
    //jugar videojuegos
    var videojuegosSi = document.getElementById("videojuegosSi").checked;
    const contenedorVideojuegos = document.getElementById("noVideojuegos");
    //redes sociales
    var redesSocialesSi = document.getElementById("redesSocialesSi").checked;
    const contenedorRedesSociales = document.getElementById("noRedesSociales");

    //***** vida social*****
    //salir con amigos
    var salirConAmigosSi = document.getElementById("salirConAmigosSi").checked;
    const contenedorSalirConAmigos = document.getElementById("noSalirConAmigos");
    //salir con novi@
    var salirConNovioSi = document.getElementById("salirConNovioSi").checked;
    const contenedorSalirConNovio = document.getElementById("noSalirConNovio");
    //evento social
    var eventoSocialSi = document.getElementById("eventoSocialSi").checked;
    const contenedorEventoSocial = document.getElementById("noEventoSocial");

    //***** otras actividades*****
    //Hacer ejercicio/deporte
    var HacerEjercicioSi = document.getElementById("HacerEjercicioSi").checked;
    const contenedorNoHaceEjercicio = document.getElementById("noHaceEjercicio");
    //trabajar
    var trabajarSi = document.getElementById("trabajarSi").checked;
    const contenedorNoTrabaja = document.getElementById("noTrabaja");
    //hacer compras
    var hacerComprasSi = document.getElementById("hacerComprasSi").checked;
    const contenedorNoHaceCompras = document.getElementById("noHaceCompras");
    //pasear perro
    var pasearPerroSi = document.getElementById("pasearPerroSi").checked;
    const contenedorNoPaseaPerro = document.getElementById("noPaseaPerro");
    //bañar perro
    var bañarPerroSi = document.getElementById("bañarPerroSi").checked;
    const contenedorNoBañaPerro = document.getElementById("noBañaPerro");



    if (preparaComidaSi) { //preparar comida

        contenedorPreparaComida.style.display = "block";

    } else {
        contenedorPreparaComida.style.display = "none";
    }

    if (lavaTrastesSi) { //lavar trastes

        contenedorLavaTrastes.style.display = "block";

    } else {
        contenedorLavaTrastes.style.display = "none";
    }

    if (barreSi) { //barrer

        contenedorBarre.style.display = "block";

    } else {
        contenedorBarre.style.display = "none";
    }

    if (trapeaSi) { //trapear

        contenedorTrapea.style.display = "block";

    } else {
        contenedorTrapea.style.display = "none";
    }

    if (lavarRopaSi) { //lavar ropa

        contenedorLavarRopa.style.display = "block";

    } else {
        contenedorLavarRopa.style.display = "none";
    }

    if (maquillarseSi) { //maquillarse

        contenedorMaquillarse.style.display = "block";
        contenedorDesmaquillarse.style.display = "block";

    } else {
        contenedorMaquillarse.style.display = "none";
        contenedorDesmaquillarse.style.display = "none";
    }

    if (clasesVirtualesSi) { //clases virtuales

        contenedorClasesVirtuales.style.display = "block";

    } else {
        contenedorClasesVirtuales.style.display = "none";
    }

    if (clasesPresencialesSi) { //clases presenciales

        contenedorClasesPresenciales.style.display = "block";
        contenedorClasesPresenciales2.style.display = "block";
    } else {
        contenedorClasesPresenciales.style.display = "none";
        contenedorClasesPresenciales2.style.display = "none";
    }

    if (estudioAutodidactaSi) { //estudio fuera del horario de clases

        contenedorEstudioAutodidacta.style.display = "block";

    } else {
        contenedorEstudioAutodidacta.style.display = "none";
    }

    if (avanceProyectoSi) { //avance proyecto

        contenedorAvanceProyecto.style.display = "block";

    } else {
        contenedorAvanceProyecto.style.display = "none";
    }

    if (verTvSerieSi) { //ver TV o serie

        contenedorVerTvSerie.style.display = "block";

    } else {
        contenedorVerTvSerie.style.display = "none";
    }

    if (videojuegosSi) { //jugar videojuegos

        contenedorVideojuegos.style.display = "block";

    } else {
        contenedorVideojuegos.style.display = "none";
    }

    if (redesSocialesSi) { //redes sociales

        contenedorRedesSociales.style.display = "block";

    } else {
        contenedorRedesSociales.style.display = "none";
    }

    if (salirConAmigosSi) { //salir con amigos

        contenedorSalirConAmigos.style.display = "block";

    } else {
        contenedorSalirConAmigos.style.display = "none";
    }

    if (salirConNovioSi) { //salir con novi@

        contenedorSalirConNovio.style.display = "block";

    } else {
        contenedorSalirConNovio.style.display = "none";
    }

    if (eventoSocialSi) { //evento social

        contenedorEventoSocial.style.display = "block";

    } else {
        contenedorEventoSocial.style.display = "none";
    }

    if (HacerEjercicioSi) { //hacer ejercicio/deporte

        contenedorNoHaceEjercicio.style.display = "block";

    } else {
        contenedorNoHaceEjercicio.style.display = "none";
    }

    if (trabajarSi) { //trabajar

        contenedorNoTrabaja.style.display = "block";

    } else {
        contenedorNoTrabaja.style.display = "none";
    }

    if (hacerComprasSi) { //hacer compras

        contenedorNoHaceCompras.style.display = "block";

    } else {
        contenedorNoHaceCompras.style.display = "none";
    }

    if (pasearPerroSi) { //pasear perro

        contenedorNoPaseaPerro.style.display = "block";

    } else {
        contenedorNoPaseaPerro.style.display = "none";
    }

    if (bañarPerroSi) { //bañar perro

        contenedorNoBañaPerro.style.display = "block";

    } else {
        contenedorNoBañaPerro.style.display = "none";
    }

}