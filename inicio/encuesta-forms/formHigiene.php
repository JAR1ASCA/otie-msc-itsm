<!-- ***************************************************-->
<!-- *************************************************** -->
<!-- ********* Contenedor Formulario Higiene personal ********** -->
<!-- *************************************************** -->
<!-- *************************************************** -->

<div id="containerHigiene" class="containerBase">

    <form class="formulario" action="" method="POST">

        <h1>Encuesta OTI:E</h1>

        <h2>III. Higiene personal</h2>

        <br>
        <h3>Tomar un baño</h3>
        <br>
        <ol>
            <!-- <li>¿Usted lava trastes?</li>
                < !-- <div class="ejemplo"></div> -- >

                <div class="radio">
                    <input type="radio" name="lavaTrastesSioNo" id="lavaTrastesSi" value="S" onclick="main()">
                    <label for="lavaTrastesSi">Si</label>

                    <input type="radio" name="lavaTrastesSioNo" id="lavaTrastesNo" value="N" onclick="main()">
                    <label for="lavaTrastesNo">No</label>
                </div> -->

            <div id="noSeBaña">
                <li>¿Tiene días determinados para tomar un baño?</li>
                <div class="ejemplo">Ejemplo: cada tercer día.</div>

                <div class="radio">
                    <input type="radio" name="bañarseDiaFijo" id="bañarseDiaFijoSi" value="S" required>
                    <label for="bañarseDiaFijoSi">Si</label>

                    <input type="radio" name="bañarseDiaFijo" id="bañarseDiaFijoNo" value="N" required>
                    <label for="bañarseDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para tomar un baño?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 6 de la mañana.</div>

                <div class="radio">
                    <input type="radio" name="bañarseHorarioFijo" id="bañarseHorarioFijoSi" value="S" onclick="horario()" required>
                    <label for="bañarseHorarioFijoSi">Si</label>

                    <input type="radio" name="bañarseHorarioFijo" id="bañarseHorarioFijoNo" value="N" onclick="horario()" required>
                    <label for="bañarseHorarioFijoNo">No</label>
                </div>


                <div id="bañarseHorarioFijo_si" ">
                    <li>¿Cúal es el horario en el que suele tomar un baño?</li>
                    <div class="ejemplo">Ejemplo: si lo toma entre 6 y 7 de la mañana, coloque "6".</div>

                    <div class="input-container">
                        <input type="number" name="bañarse_horario" placeholder="6" min="1" max="12">
                        <select name="bañarse_horario1" id="bañarse_horarioAmPm">

                            <option value="am">AM</option>
                            <option value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="bañarseHorarioFijo_no" >
                    <li>¿Entre qué horas suele tomar un baño?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre 6hrs y 23hrs.</div>

                    <div class="input-container">
                        <input type="number" name="bañarse_horario100" placeholder="6" min="0" max="21">hrs
                        <input type="number" name="bañarse_horario200" placeholder="23" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="bañarse_duracionHrs" placeholder="0" min="0" max="5" required> Horas
                    <input type="number" name="bañarse_duracionMin" placeholder="30 " min="0" max="59" required> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Maquillarse</h3>
        <br>
        <ol>
            <li>¿Usted se maquilla?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="maquillarseSioNo" id="maquillarseSi" value="S" onclick="main()" required>
                <label for="maquillarseSi">Si</label>

                <input type="radio" name="maquillarseSioNo" id="maquillarseNo" value="N" onclick="main()" required>
                <label for="maquillarseNo">No</label>
            </div>

            <div id="noSeMaquilla">
                <li>¿Tiene días determinados para maquillarse?</li>
                <div class="ejemplo">Ejemplo: cada día.</div>

                <div class="radio">
                    <input type="radio" name="maquillarseDiaFijo" id="maquillarseDiaFijoSi" value="S">
                    <label for="maquillarseDiaFijoSi">Si</label>

                    <input type="radio" name="maquillarseDiaFijo" id="maquillarseDiaFijoNo" value="N">
                    <label for="maquillarseDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para maquillarse?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 6 de la mañana.</div>

                <div class="radio">
                    <input type="radio" name="maquillarseHorarioFijo" id="maquillarseHorarioFijoSi" onclick="horario()" value="S">
                    <label for="maquillarseHorarioFijoSi">Si</label>

                    <input type="radio" name="maquillarseHorarioFijo" id="maquillarseHorarioFijoNo" onclick="horario()" value="N">
                    <label for="maquillarseHorarioFijoNo">No</label>
                </div>


                <div id="maquillarseHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: si se maquilla entre 6 y 7 de la mañana, coloque "6".</div>

                    <div class="input-container">
                        <input type="number" name="maquillarse_horario" placeholder="6" min="1" max="12">
                        <select name="maquillarse_horario1" id="maquillarse_horarioAmPm">

                            <option value="am">AM</option>
                            <option value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="maquillarseHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre 6hrs y 17hrs.</div>

                    <div class="input-container">
                        <input type="number" name="maquillarse_horario100" placeholder="6" min="0" max="21">hrs
                        <input type="number" name="maquillarse_horario200" placeholder="17" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="maquillarse_duracionHrs" placeholder="0" min="0" max="15"> Horas
                    <input type="number" name="maquillarse_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <div id="noSeDesmaquilla">
            <br>
            <h3>Desmaquillarse</h3>
            <br>
            <ol>

                <li>¿Tiene días determinados para desmaquillarse?</li>
                <div class="ejemplo">Ejemplo: cada día.</div>

                <div class="radio">
                    <input type="radio" name="desmaquillarseDiaFijo" id="desmaquillarseDiaFijoSi" value="S">
                    <label for="desmaquillarseDiaFijoSi">Si</label>

                    <input type="radio" name="desmaquillarseDiaFijo" id="desmaquillarseDiaFijoNo" value="N">
                    <label for="desmaquillarseDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para desmaquillarse?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 9 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="desmaquillarseHorarioFijo" id="desmaquillarseHorarioFijoSi" onclick="horario()" value="S">
                    <label for="desmaquillarseHorarioFijoSi">Si</label>

                    <input type="radio" name="desmaquillarseHorarioFijo" id="desmaquillarseHorarioFijoNo" onclick="horario()" value="N">
                    <label for="desmaquillarseHorarioFijoNo">No</label>
                </div>


                <div id="desmaquillarseHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: si los lava entre 9 y 10 de la noche, coloque "9".</div>

                    <div class="input-container">
                        <input type="number" name="desmaquillarse_horario" placeholder="9" min="1" max="12">
                        <select name="desmaquillarse_horario1" id="desmaquillarse_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="desmaquillarseHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre 19hrs y 22hrs.</div>

                    <div class="input-container">
                        <input type="number" name="desmaquillarse_horario100" placeholder="19" min="0" max="21">hrs
                        <input type="number" name="desmaquillarse_horario200" placeholder="22" min="2" max="23">hrs

                    </div>
                </div>



                <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="desmaquillarse_duracionHrs" placeholder="0" min="0" max="15"> Horas
                    <input type="number" name="desmaquillarse_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>

            </ol>
        </div>

        <div class="divBtn">
            <input type="submit" class="btn" value="Siguiente" name="btnHigiene" />
        </div>
    </form>
</div>
<script src="encuesta.js"></script>
<script src="encuestaHorarios.js"></script>
