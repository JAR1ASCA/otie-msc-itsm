<!-- ***************************************************-->
<!-- *************************************************** -->
<!-- ********* Contenedor Formulario Vida social ********** -->
<!-- *************************************************** -->
<!-- *************************************************** -->

<div id="containerVidaSocial" class="containerBase">

    <form class="formulario" action="" method="post">

        <h1>Encuesta OTI:E</h1>

        <h2>VI. Vida social</h2>

        <br>
        <h3>Salir con amigos(as)</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a salir con sus amigos o amigas?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="salirConAmigosSioNo" id="salirConAmigosSi" value="S" onclick="main()" required>
                <label for="salirConAmigosSi">Si</label>

                <input type="radio" name="salirConAmigosSioNo" id="salirConAmigosNo" value="N" onclick="main()" required>
                <label for="salirConAmigosNo">No</label>
            </div>

            <div id="noSalirConAmigos">
                <li>¿Tiene días determinados para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: el fin de semana.</div>

                <div class="radio">
                    <input type="radio" name="salirConAmigosDiaFijo" id="salirConAmigosDiaFijoSi" value="S">
                    <label for="salirConAmigosDiaFijoSi">Si</label>

                    <input type="radio" name="salirConAmigosDiaFijo" id="salirConAmigosDiaFijoNo" value="N">
                    <label for="salirConAmigosDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para esta actividad?</li>
                <div class="ejemplo">Ejemplo: a partir de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="salirConAmigosHorarioFijo" id="salirConAmigosHorarioFijoSi" onclick="horario()" value="S">
                    <label for="salirConAmigosHorarioFijoSi">Si</label>

                    <input type="radio" name="salirConAmigosHorarioFijo" id="salirConAmigosHorarioFijoNo" onclick="horario()" value="N">
                    <label for="salirConAmigosHorarioFijoNo">No</label>
                </div>


                <div id="salirConAmigosHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele dar inicio este acto?</li>
                    <div class="ejemplo">Ejemplo: a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="salirConAmigos_horario" placeholder="4" min="1" max="12">
                        <select name="salirConAmigos_horario1" id="salirConAmigos_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="salirConAmigosHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 16hrs y 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="salirConAmigos_horario100" placeholder="16" min="0" max="21">hrs
                        <input type="number" name="salirConAmigos_horario200" placeholder="18" min="2" max="23">hrs

                    </div>
                </div>



                <li>¿Cuánto tiempo dura este evento?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="salirConAmigos_duracionHrs" placeholder="2" min="0" max="24"> Horas
                    <input type="number" name="salirConAmigos_duracionMin" placeholder="0" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <br>
        <h3>Salir con novio(a)</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a salir con su novio o novia?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="salirConNovioSioNo" id="salirConNovioSi" value="S" onclick="main()" required>
                <label for="salirConNovioSi">Si</label>

                <input type="radio" name="salirConNovioSioNo" id="salirConNovioNo" value="N" onclick="main()" required>
                <label for="salirConNovioNo">No</label>
            </div>

            <div id="noSalirConNovio">
                <li>¿Tiene días determinados para salir?</li>
                <div class="ejemplo">Ejemplo: los fines de semana.</div>

                <div class="radio">
                    <input type="radio" name="salirConNovioDiaFijo" id="salirConNovioDiaFijoSi" value="S">
                    <label for="salirConNovioDiaFijoSi">Si</label>

                    <input type="radio" name="salirConNovioDiaFijo" id="salirConNovioDiaFijoNo" value="N">
                    <label for="salirConNovioDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para salir?</li>
                <div class="ejemplo">Ejemplo: a partir de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="salirConNovioHorarioFijo" id="salirConNovioHorarioFijoSi" onclick="horario()" value="S">
                    <label for="salirConNovioHorarioFijoSi">Si</label>

                    <input type="radio" name="salirConNovioHorarioFijo" id="salirConNovioHorarioFijoNo" onclick="horario()" value="N">
                    <label for="salirConNovioHorarioFijoNo">No</label>
                </div>


                <div id="salirConNovioHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="salirConNovio_horario" placeholder="4" min="1" max="12">
                        <select name="salirConNovio_horario1" id="salirConNovio_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="salirConNovioHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 16hrs y las 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="salirConNovio_horario100" placeholder="16" min="0" max="21"> </select>hrs
                        <input type="number" name="salirConNovio_horario200" placeholder="18" min="2" max="23"> </select>hrs

                    </div>
                </div>



                <li>¿Cuánto tiempo dedica a este evento?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="salirConNovio_duracionHrs" placeholder="2" min="0" max="24"> Horas
                    <input type="number" name="salirConNovio_duracionMin" placeholder="0 " min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Evento social</h3>
        <br>
        <ol>
            <li>¿Usted dedica tiempo a salir a eventos sociales?</li>
            <div class="ejemplo">Ejemplo: fiestas, festivales, conciertos, bailables, carnavales, desfiles.</div>

            <div class="radio">
                <input type="radio" name="eventoSocialSioNo" id="eventoSocialSi" value="S" onclick="main()" required>
                <label for="eventoSocialSi">Si</label>

                <input type="radio" name="eventoSocialSioNo" id="eventoSocialNo" value="N" onclick="main()" required>
                <label for="eventoSocialNo">No</label>
            </div>

            <div id="noEventoSocial">
                <li>¿Tiene días determinados para salir?</li>
                <div class="ejemplo">Ejemplo: los fines de semana.</div>

                <div class="radio">
                    <input type="radio" name="eventoSocialDiaFijo" id="eventoSocialDiaFijoSi" value="S">
                    <label for="eventoSocialDiaFijoSi">Si</label>

                    <input type="radio" name="eventoSocialDiaFijo" id="eventoSocialDiaFijoNo" value="N">
                    <label for="eventoSocialDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 6 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="eventoSocialHorarioFijo" id="eventoSocialHorarioFijoSi" onclick="horario()" value="S">
                    <label for="eventoSocialHorarioFijoSi">Si</label>

                    <input type="radio" name="eventoSocialHorarioFijo" id="eventoSocialHorarioFijoNo" onclick="horario()" value="N">
                    <label for="eventoSocialHorarioFijoNo">No</label>
                </div>


                <div id="eventoSocialHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 6 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="eventoSocial_horario" placeholder="6" min="1" max="12">
                        <select name="eventoSocial_horario1" id="eventoSocial_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="eventoSocialHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horario suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 16hrs y 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="eventoSocial_horario100" placeholder="16" min="0" max="21">hrs
                        <input type="number" name="eventoSocial_horario200" placeholder="18" min="2" max="23">hrs

                    </div>
                </div>



                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="eventoSocial_duracionHrs" placeholder="0" min="0" max="24"> Horas
                    <input type="number" name="eventoSocial_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <div class="divBtn">
            <input type="submit" class="btn" value="Siguiente" name="btnVidaSocial" />
        </div>
    </form>
</div>
<script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>