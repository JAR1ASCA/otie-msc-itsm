 <!-- ***************************************************-->
 <!-- *************************************************** -->
 <!-- ********* Contenedor Formulario Limpieza ********** -->
 <!-- *************************************************** -->
 <!-- *************************************************** -->

 <div id="containerLimpieza" class="containerBase">

     <form class="formulario" action="" method="post">

         <h1>Encuesta OTI:E</h1>

         <h2>II. Limpieza</h2>

         <br>
         <h3>Lavar trastes</h3>
         <br>
         <ol>
             <li>¿Usted lava trastes?</li>
             <!-- <div class="ejemplo"></div> -->

             <div class="radio">
                 <input type="radio" name="lavarTrastesSioNo" id="lavaTrastesSi" value="S" onclick="main()" required>
                 <label for="lavaTrastesSi">Si</label>

                 <input type="radio" name="lavarTrastesSioNo" id="lavaTrastesNo" value="N" onclick="main()" required>
                 <label for="lavaTrastesNo">No</label>
             </div>

             <div id="noLavaTrastes">
                 <li>¿Tiene días determinados para lavar los trastes?</li>
                 <div class="ejemplo">Ejemplo: cada miercoles y viernes.</div>

                 <div class="radio">
                     <input type="radio" name="lavarTrastesDiaFijo" id="lavarTrastesDiaFijoSi" value="S">
                     <label for="lavarTrastesDiaFijoSi">Si</label>

                     <input type="radio" name="lavarTrastesDiaFijo" id="lavarTrastesDiaFijoNo" value="N">
                     <label for="lavarTrastesDiaFijoNo">No</label>
                 </div>

                 <li>¿Tiene un horario fijo para lavar los trastes?</li>
                 <div class="ejemplo">Ejemplo: alrededor de las 3 de la tarde.</div>

                 <div class="radio">
                     <input type="radio" name="lavarTrastesHorarioFijo" id="lavarTrastesHorarioFijoSi" onclick="horario()" value="S">
                     <label for="lavarTrastesHorarioFijoSi">Si</label>

                     <input type="radio" name="lavarTrastesHorarioFijo" id="lavarTrastesHorarioFijoNo" onclick="horario()" value="N">
                     <label for="lavarTrastesHorarioFijoNo">No</label>
                 </div>

                 <div id="lavarTrastesHorarioFijo_si" style="display: none;">

                     <li>¿Cúal es el horario en el que suele lavar los trastes?</li>
                     <div class="ejemplo">Ejemplo: si los lava entre 3 y 4 de la tarde, coloque "3".</div>

                     <div class="input-container">
                         <input type="number" name="lavarTrastes_horario" placeholder="3" min="1" max="12">
                         <select name="lavarTrastes_horario1" id="lavarTrastes_horarioAmPm">

                             <option value="am">AM</option>
                             <option selected value="pm">PM</option>
                         </select>
                     </div>
                 </div>

                 <div id="lavarTrastesHorarioFijo_no" style="display: none;">
                     <li>¿Entre qué horas suele lavar los trastes?</li>
                     <div class="ejemplo">Nota: formato de 24hrs.</div>
                     <div class="ejemplo">Ejemplo: entre las 13hrs y 16hrs.</div>

                     <div class="input-container">
                         <input type="number" name="lavarTrastes_horario100" placeholder="13" min="0" max="21">hrs
                         <input type="number" name="lavarTrastes_horario200" placeholder="16" min="2" max="23">hrs
                     </div>
                 </div>



                 <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                 <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                 <div class="input-container ">
                     <input type="number" name="lavarTrastes_duracionHrs" placeholder="0" min="0" max="15"> Horas
                     <input type="number" name="lavarTrastes_duracionMin" placeholder="30 " min="0" max="59"> Minutos
                 </div>
             </div>
         </ol>


         <br>
         <h3>Barrer</h3>
         <br>
         <ol>
             <li>¿Usted barre?</li>
             <!-- <div class="ejemplo"></div> -->

             <div class="radio">
                 <input type="radio" name="barrerSioNo" id="barreSi" value="S" onclick="main()" required>
                 <label for="barreSi">Si</label>

                 <input type="radio" name="barrerSioNo" id="barreNo" value="N" onclick="main()" required>
                 <label for="barreNo">No</label>
             </div>

             <div id="noBarre">
                 <li>¿Tiene días determinados para barrer?</li>
                 <div class="ejemplo">Ejemplo: cada miercoles y viernes.</div>

                 <div class="radio">
                     <input type="radio" name="barrerDiaFijo" id="barrerDiaFijoSi" value="S">
                     <label for="barrerDiaFijoSi">Si</label>

                     <input type="radio" name="barrerDiaFijo" id="barrerDiaFijoNo" value="N">
                     <label for="barrerDiaFijoNo">No</label>
                 </div>

                 <li>¿Tiene un horario fijo para barrer?</li>
                 <div class="ejemplo">Ejemplo: alrededor de las 3 de la tarde.</div>

                 <div class="radio">
                     <input type="radio" name="barrerHorarioFijo" id="barrerHorarioFijoSi" onclick="horario()" value="S">
                     <label for="barrerHorarioFijoSi">Si</label>

                     <input type="radio" name="barrerHorarioFijo" id="barrerHorarioFijoNo" onclick="horario()" value="N">
                     <label for="barrerHorarioFijoNo">No</label>
                 </div>

                 <div id="barrerHorarioFijo_si" style="display: none;">
                     <li>¿Cúal es el horario en el que suele barrer?</li>
                     <div class="ejemplo">Ejemplo: si los lava entre 3 y 4 de la tarde, coloque "3".</div>

                     <div class="input-container">
                         <input type="number" name="barrer_horario" placeholder="3" min="1" max="12">
                         <select name="barrer_horario1" id="barrer_horarioAmPm">

                             <option value="am">AM</option>
                             <option selected value="pm">PM</option>
                         </select>
                     </div>
                 </div>

                 <div id="barrerHorarioFijo_no" style="display: none;">
                     <li>¿Entre qué horas suele barrer?</li>
                     <div class="ejemplo">Nota: formato de 24hrs.</div>
                     <div class="ejemplo">Ejemplo: entre 13hrs y 16hrs.</div>

                     <div class="input-container">
                         <input type="number" name="barrer_horario100" placeholder="13" min="0" max="21">hrs
                         <input type="number" name="barrer_horario200" placeholder="16" min="2" max="23">hrs

                     </div>
                 </div>



                 <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                 <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                 <div class="input-container ">
                     <input type="number" name="barrer_duracionHrs" placeholder="0" min="0" max="15"> Horas
                     <input type="number" name="barrer_duracionMin" placeholder="30 " min="0" max="59"> Minutos
                 </div>
             </div>
         </ol>

         <br>
         <h3>Trapear</h3>
         <br>
         <ol>
             <li>¿Usted trapea?</li>
             <!-- <div class="ejemplo"></div> -->

             <div class="radio">
                 <input type="radio" name="trapearSioNo" id="trapeaSi" value="S" onclick="main()" required>
                 <label for="trapeaSi">Si</label>

                 <input type="radio" name="trapearSioNo" id="trapeaNo" value="N" onclick="main()" required>
                 <label for="trapeaNo">No</label>
             </div>

             <div id="noTrapea">

                 <li>¿Tiene días determinados para trapear?</li>
                 <div class="ejemplo">Ejemplo: cada miercoles y viernes.</div>

                 <div class="radio">
                     <input type="radio" name="trapearDiaFijo" id="trapearDiaFijoSi" value="S">
                     <label for="trapearDiaFijoSi">Si</label>

                     <input type="radio" name="trapearDiaFijo" id="trapearDiaFijoNo" value="N">
                     <label for="trapearDiaFijoNo">No</label>
                 </div>

                 <li>¿Tiene un horario fijo para trapear?</li>
                 <div class="ejemplo">Ejemplo: alrededor de las 10 de la mañana.</div>

                 <div class="radio">
                     <input type="radio" name="trapearHorarioFijo" id="trapearHorarioFijoSi" onclick="horario()" value="S">
                     <label for="trapearHorarioFijoSi">Si</label>

                     <input type="radio" name="trapearHorarioFijo" id="trapearHorarioFijoNo" onclick="horario()" value="N">
                     <label for="trapearHorarioFijoNo">No</label>
                 </div>


                 <div id="trapearHorarioFijo_si" style="display: none;">
                     <li>¿Cúal es el horario en el que suele trapear?</li>
                     <div class="ejemplo">Ejemplo: si trapea entre 10 y 11 de la mañana, coloque "10".</div>

                     <div class="input-container">
                         <input type="number" name="trapear_horario" placeholder="10" min="1" max="12">
                         <select name="trapear_horario1" id="trapear_horarioAmPm">

                             <option value="am">AM</option>
                             <option value="pm">PM</option>
                         </select>
                     </div>
                 </div>

                 <div id="trapearHorarioFijo_no" style="display: none;">
                     <li>¿Entre qué horas suele trapear?</li>
                     <div class="ejemplo">Nota: formato de 24hrs.</div>
                     <div class="ejemplo">Ejemplo: entre 8hrs y 11hrs.</div>

                     <div class="input-container">
                         <input type="number" name="trapear_horario100" placeholder="8" min="0" max="21">hrs
                         <input type="number" name="trapear_horario200" placeholder="11" min="2" max="23">hrs

                     </div>
                 </div>


                 <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                 <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                 <div class="input-container ">
                     <input type="number" name="trapear_duracionHrs" placeholder="0" min="0" max="15"> Horas
                     <input type="number" name="trapear_duracionMin" placeholder="30 " min="0" max="59"> Minutos
                 </div>
             </div>
         </ol>

         <br>
         <h3>Ordenar/recoger cuarto</h3>
         <br>
         <ol>
             <!-- <li>¿Usted ordena su cuarto?</li>
                <! -- <div class="ejemplo"></div> -- >

                <div class="radio">
                    <input type="radio" name="ordenaCuartoSioNo" id="ordenaCuartoSi" value="S" onclick="main()">
                    <label for="ordenaCuartoSi">Si</label>

                    <input type="radio" name="ordenaCuartoSioNo" id="ordenaCuartoNo" value="N" onclick="main()">
                    <label for="ordenaCuartoNo">No</label>
                </div> -->

             <div id="noOrdenaCuarto">

                 <li>¿Tiene días determinados para ordenar su cuarto?</li>
                 <div class="ejemplo">Ejemplo: cada miercoles y viernes.</div>

                 <div class="radio">
                     <input type="radio" name="ordenarCuartoDiaFijo" id="ordenarCuartoDiaFijoSi" value="S" required>
                     <label for="ordenarCuartoDiaFijoSi">Si</label>

                     <input type="radio" name="ordenarCuartoDiaFijo" id="ordenarCuartoDiaFijoNo" value="N" required>
                     <label for="ordenarCuartoDiaFijoNo">No</label>
                 </div>

                 <li>¿Tiene un horario fijo para ordenar su cuarto?</li>
                 <div class="ejemplo">Ejemplo: alrededor de las 5 de la tarde.</div>

                 <div class="radio">
                     <input type="radio" name="ordenarCuartoHorarioFijo" id="ordenarCuartoHorarioFijoSi" value="S" onclick="horario()" required>
                     <label for="ordenarCuartoHorarioFijoSi">Si</label>

                     <input type="radio" name="ordenarCuartoHorarioFijo" id="ordenarCuartoHorarioFijoNo" value="N" onclick="horario()" required>
                     <label for="ordenarCuartoHorarioFijoNo">No</label>
                 </div>


                 <div id="ordenarCuartoHorarioFijo_si" style="display: none;">
                     <li>¿Cúal es el horario en el que suele ordenar su cuarto?</li>
                     <div class="ejemplo">Ejemplo: si ordena entre 5 y 6 de la tarde, coloque "5".</div>

                     <div class="input-container">
                         <input type="number" name="ordenarCuarto_horario" placeholder="5" min="1" max="12">
                         <select name="ordenarCuarto_horario1" id="ordenarCuarto_horarioAmPm">

                             <option value="am">AM</option>
                             <option selected value="pm">PM</option>
                         </select>
                     </div>
                 </div>

                 <div id="ordenarCuartoHorarioFijo_no" style="display: none;">
                     <li>¿Entre qué horas suele ordenar su cuarto?</li>
                     <div class="ejemplo">Nota: formato de 24hrs.</div>
                     <div class="ejemplo">Ejemplo: entre 13hrs y 15hrs.</div>

                     <div class="input-container">
                         <input type="number" name="ordenarCuarto_horario100" placeholder="13" min="0" max="21">hrs
                         <input type="number" name="ordenarCuarto_horario200" placeholder="15" min="2" max="23">hrs

                     </div>
                 </div>


                 <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                 <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                 <div class="input-container ">
                     <input type="number" name="ordenarCuarto_duracionHrs" placeholder="0" min="0" max="15" required> Horas
                     <input type="number" name="ordenarCuarto_duracionMin" placeholder="30 " min="0" max="59" required> Minutos
                 </div>
             </div>
         </ol>


         <br>
         <h3>Lavar ropa</h3>
         <br>
         <ol>
             <li>¿Usted lava ropa?</li>
             <!-- <div class="ejemplo"></div> -->

             <div class="radio">
                 <input type="radio" name="lavarRopaSioNo" id="lavarRopaSi" value="S" onclick="main()" required>
                 <label for="lavarRopaSi">Si</label>

                 <input type="radio" name="lavarRopaSioNo" id="lavarRopaNo" value="N" onclick="main()" required>
                 <label for="lavarRopaNo">No</label>
             </div>

             <div id="noLavaRopa">

                 <li>¿Tiene días determinados para lavar ropa?</li>
                 <div class="ejemplo">Ejemplo: cada miercoles y viernes.</div>

                 <div class="radio">
                     <input type="radio" name="lavarRopaDiaFijo" id="lavarRopaDiaFijoSi" value="S">
                     <label for="lavarRopaDiaFijoSi">Si</label>

                     <input type="radio" name="lavarRopaDiaFijo" id="lavarRopaDiaFijoNo" value="N">
                     <label for="lavarRopaDiaFijoNo">No</label>
                 </div>

                 <li>¿Tiene un horario fijo para lavar ropa?</li>
                 <div class="ejemplo">Ejemplo: alrededor de las 10 de la mañana.</div>

                 <div class="radio">
                     <input type="radio" name="lavarRopaHorarioFijo" id="lavarRopaHorarioFijoSi" onclick="horario()" value="S">
                     <label for="lavarRopaHorarioFijoSi">Si</label>

                     <input type="radio" name="lavarRopaHorarioFijo" id="lavarRopaHorarioFijoNo" onclick="horario()" value="N">
                     <label for="lavarRopaHorarioFijoNo">No</label>
                 </div>


                 <div id="lavarRopaHorarioFijo_si" style="display: none;">
                     <li>¿Cúal es el horario en el que suele lavar ropa?</li>
                     <div class="ejemplo">Ejemplo: si trapea entre 10 y 11 de la mañana, coloque "10".</div>

                     <div class="input-container">
                         <input type="number" name="lavarRopa_horario" placeholder="10" min="1" max="12">
                         <select name="lavarRopa_horario1" id="lavarRopa_horarioAmPm">

                             <option value="am">AM</option>
                             <option value="pm">PM</option>
                         </select>
                     </div>
                 </div>

                 <div id="lavarRopaHorarioFijo_no" style="display: none;">
                     <li>¿Entre qué horas suele lavar ropa?</li>
                     <div class="ejemplo">Nota: formato de 24hrs.</div>
                     <div class="ejemplo">Ejemplo: entre 10hrs y 15hrs.</div>

                     <div class="input-container">
                         <input type="number" name="lavarRopa_horario100" placeholder="10" min="0" max="21">hrs
                         <input type="number" name="lavarRopa_horario200" placeholder="15" min="2" max="23">hrs

                     </div>
                 </div>


                 <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                 <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                 <div class="input-container ">
                     <input type="number" name="lavarRopa_duracionHrs" placeholder="0" min="0" max="15"> Horas
                     <input type="number" name="lavarRopa_duracionMin" placeholder="30 " min="0" max="59"> Minutos
                 </div>
             </div>
         </ol>

         <div>
             <!-- <div class="divBtn">
                    <input type="submit" class="btn" value="Regresar" name="btnLimpiezaRegresar">
                </div> -->
             <div class="divBtn">
                 <input type="submit" class="btn" value="Siguiente" name="btnLimpieza" />
             </div>
         </div>

     </form>

 </div>
 <script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>