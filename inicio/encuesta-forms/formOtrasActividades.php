<!-- ***************************************************-->
<!-- *************************************************** -->
<!-- ********* Contenedor Formulario Otras Actividades ********** -->
<!-- *************************************************** -->
<!-- *************************************************** -->

<div id="containerOtrasActividades" class="containerBase">

    <form class="formulario" action="" method="post">

        <h1>Encuesta OTI:E</h1>

        <h2>VII. Otras actividades</h2>

        <br>
        <h3>Hacer ejercicio, practicar deporte, actividad artistica o cultural</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a esta actividad?</li>
            <div class="ejemplo">Ejemplo: despues de clases, el fin de semana.</div>

            <div class="radio">
                <input type="radio" name="HacerEjercicioSioNo" id="HacerEjercicioSi" value="S" onclick="main()" required>
                <label for="HacerEjercicioSi">Si</label>

                <input type="radio" name="HacerEjercicioSioNo" id="HacerEjercicioNo" value="N" onclick="main()" required>
                <label for="HacerEjercicioNo">No</label>
            </div>

            <div id="noHaceEjercicio">
                <li>¿Tiene días determinados para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: el fin de semana.</div>

                <div class="radio">
                    <input type="radio" name="HacerEjercicioDiaFijo" id="HacerEjercicioDiaFijoSi" value="S">
                    <label for="HacerEjercicioDiaFijoSi">Si</label>

                    <input type="radio" name="HacerEjercicioDiaFijo" id="HacerEjercicioDiaFijoNo" value="N">
                    <label for="HacerEjercicioDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para esta actividad?</li>
                <div class="ejemplo">Ejemplo: a partir de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="HacerEjercicioHorarioFijo" id="HacerEjercicioHorarioFijoSi" onclick="horario()" value="S">
                    <label for="HacerEjercicioHorarioFijoSi">Si</label>

                    <input type="radio" name="HacerEjercicioHorarioFijo" id="HacerEjercicioHorarioFijoNo" onclick="horario()" value="N">
                    <label for="HacerEjercicioHorarioFijoNo">No</label>
                </div>


                <div id="HacerEjercicioHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele dar inicio este acto?</li>
                    <div class="ejemplo">Ejemplo: a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="HacerEjercicio_horario" placeholder="4" min="1" max="12">
                        <select name="HacerEjercicio_horario1" id="HacerEjercicio_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="HacerEjercicioHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre 14hrs y 17hrs.</div>

                    <div class="input-container">
                        <input type="number" name="HacerEjercicio_horario100" placeholder="14" min="0" max="21">hrs
                        <input type="number" name="HacerEjercicio_horario200" placeholder="17" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo dura este evento?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="HacerEjercicio_duracionHrs" placeholder="2" min="0" max="24"> Horas
                    <input type="number" name="HacerEjercicio_duracionMin" placeholder="0" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Trabajar</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a trabajar fuera del horario de clases?</li>
            <div class="ejemplo">Ejemplo: despues de clases, el fin de semana.</div>

            <div class="radio">
                <input type="radio" name="trabajarSioNo" id="trabajarSi" value="S" onclick="main()" required>
                <label for="trabajarSi">Si</label>

                <input type="radio" name="trabajarSioNo" id="trabajarNo" value="N" onclick="main()" required>
                <label for="trabajarNo">No</label>
            </div>

            <div id="noTrabaja">
                <li>¿Tiene días determinados para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: el fin de semana.</div>

                <div class="radio">
                    <input type="radio" name="trabajarDiaFijo" id="trabajarDiaFijoSi" value="S">
                    <label for="trabajarDiaFijoSi">Si</label>

                    <input type="radio" name="trabajarDiaFijo" id="trabajarDiaFijoNo" value="N">
                    <label for="trabajarDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para esta actividad?</li>
                <div class="ejemplo">Ejemplo: a partir de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="trabajarHorarioFijo" id="trabajarHorarioFijoSi" onclick="horario()" value="S">
                    <label for="trabajarHorarioFijoSi">Si</label>

                    <input type="radio" name="trabajarHorarioFijo" id="trabajarHorarioFijoNo" onclick="horario()" value="N">
                    <label for="trabajarHorarioFijoNo">No</label>
                </div>


                <div id="trabajarHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele dar inicio este acto?</li>
                    <div class="ejemplo">Ejemplo: a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="trabajar_horario" placeholder="4" min="1" max="12">
                        <select name="trabajar_horario1" id="trabajar_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="trabajarHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 8hrs y 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="trabajar_horario100" placeholder="8" min="0" max="21">hrs
                        <input type="number" name="trabajar_horario200" placeholder="18" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo dura este evento?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="trabajar_duracionHrs" placeholder="2" min="0" max="24"> Horas
                    <input type="number" name="trabajar_duracionMin" placeholder="0" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <br>
        <h3>Realizar las compras</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a realizar compras?</li>
            <div class="ejemplo">Ejemplo: ir al super.</div>

            <div class="radio">
                <input type="radio" name="hacerComprasSioNo" id="hacerComprasSi" value="S" onclick="main()" required>
                <label for="hacerComprasSi">Si</label>

                <input type="radio" name="hacerComprasSioNo" id="hacerComprasNo" value="N" onclick="main()" required>
                <label for="hacerComprasNo">No</label>
            </div>

            <div id="noHaceCompras">
                <li>¿Tiene días determinados para salir?</li>
                <div class="ejemplo">Ejemplo: los fines de semana.</div>

                <div class="radio">
                    <input type="radio" name="hacerComprasDiaFijo" id="hacerComprasDiaFijoSi" value="S">
                    <label for="hacerComprasDiaFijoSi">Si</label>

                    <input type="radio" name="hacerComprasDiaFijo" id="hacerComprasDiaFijoNo" value="N">
                    <label for="hacerComprasDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para salir?</li>
                <div class="ejemplo">Ejemplo: a partir de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="hacerComprasHorarioFijo" id="hacerComprasHorarioFijoSi" onclick="horario()" value="S">
                    <label for="hacerComprasHorarioFijoSi">Si</label>

                    <input type="radio" name="hacerComprasHorarioFijo" id="hacerComprasHorarioFijoNo" onclick="horario()" value="N">
                    <label for="hacerComprasHorarioFijoNo">No</label>
                </div>

                <div id="hacerComprasHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="hacerCompras_horario" placeholder="4" min="1" max="12">
                        <select name="hacerCompras_horario1" id="hacerCompras_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="hacerComprasHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 9hrs y 11hrs.</div>

                    <div class="input-container">
                        <input type="number" name="hacerCompras_horario100" placeholder="9" min="0" max="21">hrs
                        <input type="number" name="hacerCompras_horario200" placeholder="11" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo dedica a este evento?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="hacerCompras_duracionHrs" placeholder="2" min="0" max="15"> Horas
                    <input type="number" name="hacerCompras_duracionMin" placeholder="0 " min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Pasear al perro</h3>
        <br>
        <ol>
            <li>¿Usted dedica tiempo a esta actividad?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="pasearPerroSioNo" id="pasearPerroSi" value="S" onclick="main()" required>
                <label for="pasearPerroSi">Si</label>

                <input type="radio" name="pasearPerroSioNo" id="pasearPerroNo" value="N" onclick="main()" required>
                <label for="pasearPerroNo">No</label>
            </div>

            <div id="noPaseaPerro">
                <li>¿Tiene días determinados para pasear a la mascota?</li>
                <div class="ejemplo">Ejemplo: los fines de semana.</div>

                <div class="radio">
                    <input type="radio" name="pasearPerroDiaFijo" id="pasearPerroDiaFijoSi" value="S">
                    <label for="pasearPerroDiaFijoSi">Si</label>

                    <input type="radio" name="pasearPerroDiaFijo" id="pasearPerroDiaFijoNo" value="N">
                    <label for="pasearPerroDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 6 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="pasearPerroHorarioFijo" id="pasearPerroHorarioFijoSi" onclick="horario()" value="S">
                    <label for="pasearPerroHorarioFijoSi">Si</label>

                    <input type="radio" name="pasearPerroHorarioFijo" id="pasearPerroHorarioFijoNo" onclick="horario()" value="N">
                    <label for="pasearPerroHorarioFijoNo">No</label>
                </div>


                <div id="pasearPerroHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 6 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="pasearPerro_horario" placeholder="6" min="1" max="12">
                        <select name="pasearPerro_horario1" id="pasearPerro_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="pasearPerroHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 16hrs y 19hrs.</div>

                    <div class="input-container">
                        <input type="number" name="pasearPerro_horario100" placeholder="16" min="0" max="21">hrs
                        <input type="number" name="pasearPerro_horario200" placeholder="19" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <div class="ejemplo">Ejemplo: media hora.</div>

                <div class="input-container ">
                    <input type="number" name="pasearPerro_duracionHrs" placeholder="0" min="0" max="12"> Horas
                    <input type="number" name="pasearPerro_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <br>
        <h3>Bañar al perro</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a bañar a la mascota?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="bañarPerroSioNo" id="bañarPerroSi" value="S" onclick="main()" required>
                <label for="bañarPerroSi">Si</label>

                <input type="radio" name="bañarPerroSioNo" id="bañarPerroNo" value="N" onclick="main()" required>
                <label for="bañarPerroNo">No</label>
            </div>

            <div id="noBañaPerro">
                <li>¿Tiene días determinados para hacerlo?</li>
                <div class="ejemplo">Ejemplo: los fines de semana.</div>

                <div class="radio">
                    <input type="radio" name="bañarPerroDiaFijo" id="bañarPerroDiaFijoSi" value="S">
                    <label for="bañarPerroDiaFijoSi">Si</label>

                    <input type="radio" name="bañarPerroDiaFijo" id="bañarPerroDiaFijoNo" value="N">
                    <label for="bañarPerroDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para realizar la actividad?</li>
                <div class="ejemplo">Ejemplo: a partir de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="bañarPerroHorarioFijo" id="bañarPerroHorarioFijoSi" onclick="horario()" value="S">
                    <label for="bañarPerroHorarioFijoSi">Si</label>

                    <input type="radio" name="bañarPerroHorarioFijo" id="bañarPerroHorarioFijoNo" onclick="horario()" value="N">
                    <label for="bañarPerroHorarioFijoNo">No</label>
                </div>



                <div id="bañarPerroHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="bañarPerro_horario" placeholder="4" min="1" max="12">
                        <select name="bañarPerro_horario1" id="bañarPerro_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="bañarPerroHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 10hrs y 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="bañarPerro_horario100" placeholder="10" min="0" max="21">hrs
                        <input type="number" name="bañarPerro_horario200" placeholder="18" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo dedica a este evento?</li>
                <div class="ejemplo">Ejemplo: media hora.</div>

                <div class="input-container ">
                    <input type="number" name="bañarPerro_duracionHrs" placeholder="0" min="0" max="12"> Horas
                    <input type="number" name="bañarPerro_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Dormir</h3>
        <br>
        <ol>
            <div id="noDormir">
                <li>¿Tiene un horario fijo para dormir?</li>
                <div class="ejemplo">Ejemplo: a las 10 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="dormirHorarioFijo" id="dormirHorarioFijoSi" onclick="horario()" value="S" required>
                    <label for="dormirHorarioFijoSi">Si</label>

                    <input type="radio" name="dormirHorarioFijo" id="dormirHorarioFijoNo" onclick="horario()" value="N" required>
                    <label for="dormirHorarioFijoNo">No</label>
                </div>


                <div id="dormirHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 10 de la noche.</div>

                    <div class="input-container">
                        <input type="number" name="dormir_horario" placeholder="10" min="0" max="12" >
                        <select name="dormir_horario1" id="dormir_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="dormirHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre 23hrs y 7hrs.</div>

                    <div class="input-container">
                        <input type="number" name="dormir_horario100" placeholder="23" min="0" max="23" >hrs
                        <input type="number" name="dormir_horario200" placeholder="7" min="0" max="23" >hrs

                    </div>
                </div>

                <li>¿Cuánto tiempo dedica a dormir?</li>
                <div class="ejemplo">Ejemplo: 8 horas.</div>

                <div class="input-container ">
                    <input type="number" name="dormir_duracionHrs" placeholder="8" min="0" max="24" required> Horas
                    <input type="number" name="dormir_duracionMin" placeholder="0" min="0" max="59" required> Minutos
                </div>
            </div>
        </ol>

        <div class="divBtn">
            <input type="submit" class="btn" value="Finalizar" name="btnOtrasActividades" />
        </div>
    </form>
</div>
<script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>