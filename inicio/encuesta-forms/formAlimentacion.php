    <!-- ********* Contenedor Formulario Alimentación ********** -->

    <div id="containerAlimentacion" class="containerBase">

        <form class="formulario" action="" method="POST">

            <h1>Encuesta OTI:E</h1>

            <h2>I. Alimentación</h2>

            <br>
            <h3>Desayuno</h3>
            <br>
            <ol>
                <li>¿Tiene un horario fijo para desayunar?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 9 de la mañana.</div>

                <div class="radio">
                    <input type="radio" name="desayunoHorarioFijo" id="desayunoSi" value="S" onclick="horario()" required>
                    <label for="desayunoSi">Si</label>

                    <input type="radio" name="desayunoHorarioFijo" id="desayunoNo" value="N" onclick="horario()" required>
                    <label for="desayunoNo">No</label>
                </div>

                <!--  -->
                <div id="desayunoHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele desayunar?</li>
                    <div class="ejemplo">Ejemplo: si desayuna entre 9 y 10 de de la mañana, coloque "9".</div>

                    <div class="input-container">
                        <input type="number" name="desayuno_horario" placeholder="9 " min="5" max="11" value=""> AM
                    </div>
                </div>

                <div id="desayunoHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele desayunar?</li>
                    <div class="ejemplo">Ejemplo: entre las 7 y las 11 de la mañana.</div>

                    <div class="input-container">
                        <input type="number" name="desayuno_horario1" placeholder="7 " min="5" max="9" value=""> AM
                        <input type="number" name="desayuno_horario2" placeholder="11 " min="7" max="11" value=""> AM
                    </div>
                </div>

                <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="desayuno_duracionHrs" placeholder="0" min="0" max="5" required> Horas
                    <input type="number" name="desayuno_duracionMin" placeholder="30 " min="0" max="59" required> Minutos
                </div>
            </ol>

            <br>
            <h3>Comida</h3>
            <br>
            <ol>
                <li>¿Tiene un horario fijo para comer?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 2 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="comidaHorarioFijo" id="comidaSi" value="S" onclick="horario()" required>
                    <label for="comidaSi">Si</label>

                    <input type="radio" name="comidaHorarioFijo" id="comidaNo" value="N" onclick="horario()" required>
                    <label for="comidaNo">No</label>
                </div>

                <div id="comidaHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele comer?</li>
                    <div class="ejemplo">Ejemplo: si come entre 2 y 3 de la tarde, coloque "2".</div>

                    <div class="input-container">
                        <input type="number" name="comida_horario" placeholder="2 " min="1" max="6" > PM
                    </div>
                </div>
                <div id="comidaHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele comer?</li>
                    <div class="ejemplo">Ejemplo: entre las 2 y las 5 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="comida_horario1" placeholder="2 " min="1" max="4" > PM
                        <input type="number" name="comida_horario2" placeholder="5 " min="3" max="6" > PM
                    </div>
                </div>

                <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="comida_duracionHrs" placeholder="0" min="0" max="5" required> Horas
                    <input type="number" name="comida_duracionMin" placeholder="30 " min="0" max="59" required> Minutos
                </div>
            </ol>

            <br>
            <h3>Cena</h3>
            <br>
            <ol>
                <li>¿Tiene un horario fijo para cenar?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 8 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="cenaHorarioFijo" id="cenaSi" value="S" onclick="horario()" required>
                    <label for="cenaSi">Si</label>

                    <input type="radio" name="cenaHorarioFijo" id="cenaNo" value="N" onclick="horario()" required>
                    <label for="cenaNo">No</label>
                </div>

                <div id="cenaHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele cenar?</li>
                    <div class="ejemplo">Ejemplo: si cena entre 8 y 9 de la noche, coloque "8".</div>

                    <div class="input-container">
                        <input type="number" name="cena_horario" placeholder="8 " min="7" max="11" > PM
                    </div>
                </div>

                <div id="cenaHorarioFijo_no" style="display: none;">
                    <li>¿Cúal es el horario en el que suele cenar?</li>
                    <div class="ejemplo">Ejemplo: entre las 8 y las 10 de la noche.</div>

                    <div class="input-container">
                        <input type="number" name="cena_horario1" placeholder="8 " min="7" max="9" > PM
                        <input type="number" name="cena_horario2" placeholder="10 " min="9" max="11" > PM
                    </div>
                </div>


                <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="cena_duracionHrs" placeholder="0" min="0" max="5" required> Horas
                    <input type="number" name="cena_duracionMin" placeholder="30 " min="0" max="59" required> Minutos
                </div>
            </ol>

            <br>
            <h3>Preparar alimentos</h3>
            <br>
            <ol>
                <li>¿Usted prepara su comida?</li>
                <!-- <div class="ejemplo"></div> -->

                <div class="radio">
                    <input type="radio" name="prepararComidaSioNo" id="preparaComidaSi" value="S" onclick="main()" required>
                    <label for="preparaComidaSi" onclick="main()">Si</label>

                    <input type="radio" name="prepararComidaSioNo" id="preparaComidaNo" value="N" onclick="main()" required>
                    <label for="preparaComidaNo" onclick="main()">No</label>
                </div>

                <div id="noPreparaAlimento">

                    <li>¿Tiene días determinados para preparar sus alimentos?</li>
                    <div class="ejemplo">Ejemplo: cada miercoles y viernes.</div>

                    <div class="radio">
                        <input type="radio" name="prepararComidaDiafijo" id="comidaDiaSi" value="S">
                        <label for="comidaDiaSi">Si</label>

                        <input type="radio" name="prepararComidaDiafijo" id="comidaDiaNo" value="N">
                        <label for="comidaDiaNo">No</label>
                    </div>

                    <li>¿Tiene un horario fijo para preparar su comida?</li>
                    <div class="ejemplo">Ejemplo: alrededor de la 1 de la tarde.</div>

                    <div class="radio">
                        <input type="radio" name="prepararComidaHorariofijo" id="comidaHorarioSi" value="S">
                        <label for="comidaHorarioSi">Si</label>

                        <input type="radio" name="prepararComidaHorariofijo" id="comidaHorarioNo" value="N">
                        <label for="comidaHorarioNo">No</label>
                    </div>

                    <li>¿Cúal es el horario en el que comunmente preparar su alimento? (Aproximadamente)</li>
                    <div class="ejemplo">Ejemplo: si la prepara entre la 1 y las 2 de la tarde, coloque "1".</div>
                    <div class="nota ejemplo">Nota: puede agregar diferentes horarios para la preparacion del desayuno, comida y cena.</div>


                    <div class="input-container">
                        <input type="number" name="prepararComidaHorario1" placeholder="8" min="0" max="12">
                        <select name="prepararComidaTipo1" id="prepararComidaTipo1">

                            <option selected value="am">AM</option>
                            <option value="pm">PM</option>
                        </select>

                        <input type="radio" name="prepararComidaHorario11" id="desayuno1" value="de" checked>
                        <label for="desayuno1">Desayuno</label>
                        <input type="radio" name="prepararComidaHorario11" id="comida1" value="co">
                        <label for="comida1">Comida</label>
                        <input type="radio" name="prepararComidaHorario11" id="cena1" value="ce">
                        <label for="cena1">Cena</label>
                        <input type="radio" name="prepararComidaHorario11" id="ninguno1" value="ni">
                        <label for="ninguno1">Ninguno</label>
                    </div>

                    <div class="input-container">
                        <input type="number" name="prepararComidaHorario2" placeholder="1" min="0" max="12">
                        <select name="prepararComidaTipo2" id="prepararComidaTipo2">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>

                        <input type="radio" name="prepararComidaHorario21" id="desayuno2" value="de">
                        <label for="desayuno2">Desayuno</label>
                        <input type="radio" name="prepararComidaHorario21" id="comida2" value="co" checked>
                        <label for="comida2">Comida</label>
                        <input type="radio" name="prepararComidaHorario21" id="cena2" value="ce">
                        <label for="cena2">Cena</label>
                        <input type="radio" name="prepararComidaHorario21" id="ninguno2" value="ni">
                        <label for="ninguno2">Ninguno</label>
                    </div>

                    <div class="input-container">
                        <input type="number" name="prepararComidaHorario3" placeholder="7" min="0" max="12">
                        <select name="prepararComidaTipo3" id="prepararComidaTipo3">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>

                        <input type="radio" name="prepararComidaHorario31" id="desayuno3" value="de">
                        <label for="desayuno3">Desayuno</label>
                        <input type="radio" name="prepararComidaHorario31" id="comida3" value="co">
                        <label for="comida3">Comida</label>
                        <input type="radio" name="prepararComidaHorario31" id="cena3" value="ce" checked>
                        <label for="cena3">Cena</label>
                        <input type="radio" name="prepararComidaHorario31" id="ninguno3" value="ni">
                        <label for="ninguno3">Ninguno</label>
                    </div>


                    <li>¿Aproximadamente en cuánto tiempo realiza esta actividad?</li>
                    <div class="ejemplo">Nota: Coloque la duracion de manera correspondiente a las respuestas de la pregunta anterior.</div>

                    <div class="input-container ">
                        <input type="number" name="prepararComida_duracionHrs1" placeholder="0" min="0" max="15"> Horas
                        <input type="number" name="prepararComida_duracionMin1" placeholder="30 " min="0" max="59"> Minutos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duración 1

                    </div>

                    <div class="input-container ">
                        <input type="number" name="prepararComida_duracionHrs2" placeholder="0" min="0" max="15"> Horas
                        <input type="number" name="prepararComida_duracionMin2" placeholder="30 " min="0" max="59"> Minutos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duración 2
                    </div>

                    <div class="input-container ">
                        <input type="number" name="prepararComida_duracionHrs3" placeholder="0" min="0" max="15"> Horas
                        <input type="number" name="prepararComida_duracionMin3" placeholder="30 " min="0" max="59"> Minutos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duración 3

                    </div>

                </div>
            </ol>

            <div class="divBtn">
                <input type="submit" class="btn" value="Siguiente" name="btnAlimentacion" />
            </div>

        </form>

    </div>
    <script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>