<!-- ***************************************************-->
<!-- *************************************************** -->
<!-- ********* Contenedor Formulario Entretenimiento ********** -->
<!-- *************************************************** -->
<!-- *************************************************** -->

<div id="containerEntretenimiento" class="containerBase">

    <form class="formulario" action="" method="post">

        <h1>Encuesta OTI:E</h1>

        <h2>V. Entretenimiento</h2>

        <br>
        <h3>Ver TV, serie o película</h3>
        <br>
        <ol>
            <li>¿Dedica tiempo a ver televisión, una serie, una película o videos?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="verTvSerieSioNo" id="verTvSerieSi" value="S" onclick="main()" required>
                <label for="verTvSerieSi">Si</label>

                <input type="radio" name="verTvSerieSioNo" id="verTvSerieNo" value="N" onclick="main()" required>
                <label for="verTvSerieNo">No</label>
            </div>

            <div id="noVerTvSerie">
                <li>¿Tiene días determinados para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: el fin de semana.</div>

                <div class="radio">
                    <input type="radio" name="verTvSerieDiaFijo" id="verTvSerieDiaFijoSi" value="S">
                    <label for="verTvSerieDiaFijoSi">Si</label>

                    <input type="radio" name="verTvSerieDiaFijo" id="verTvSerieDiaFijoNo" value="N">
                    <label for="verTvSerieDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para esta actividad?</li>
                <div class="ejemplo">Ejemplo: a partir de las 10 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="verTvSerieHorarioFijo" id="verTvSerieHorarioFijoSi" onclick="horario()" value="S">
                    <label for="verTvSerieHorarioFijoSi">Si</label>

                    <input type="radio" name="verTvSerieHorarioFijo" id="verTvSerieHorarioFijoNo" onclick="horario()" value="N">
                    <label for="verTvSerieHorarioFijoNo">No</label>
                </div>

                <div id="verTvSerieHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele dar inicio este acto?</li>
                    <div class="ejemplo">Ejemplo: a las 10 de la noche.</div>

                    <div class="input-container">
                        <input type="number" name="verTvSerie_horario" placeholder="10" min="1" max="12">
                        <select name="verTvSerie_horario1" id="verTvSerie_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="verTvSerieHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 20hrs y las 22hrs.</div>

                    <div class="input-container">
                        <input type="number" name="verTvSerie_horario100" placeholder="20" min="0" max="21">hrs
                        <input type="number" name="verTvSerie_horario200" placeholder="22" min="2" max="23">hrs

                    </div>
                </div>



                <li>¿Cuánto tiempo dura esta acción?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="verTvSerie_duracionHrs" placeholder="2" min="0" max="24"> Horas
                    <input type="number" name="verTvSerie_duracionMin" placeholder="0" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <br>
        <h3>Jugar videojuegos</h3>
        <br>
        <ol>
            <li>¿Juega videojuegos?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="videojuegosSioNo" id="videojuegosSi" value="S" onclick="main()" required>
                <label for="videojuegosSi">Si</label>

                <input type="radio" name="videojuegosSioNo" id="videojuegosNo" value="N" onclick="main()" required>
                <label for="videojuegosNo">No</label>
            </div>

            <div id="noVideojuegos">
                <li>¿Tiene días determinados para jugar?</li>
                <div class="ejemplo">Ejemplo: los viernes y fines de semana.</div>

                <div class="radio">
                    <input type="radio" name="videojuegosDiaFijo" id="videojuegosDiaFijoSi" value="S">
                    <label for="videojuegosDiaFijoSi">Si</label>

                    <input type="radio" name="videojuegosDiaFijo" id="videojuegosDiaFijoNo" value="N">
                    <label for="videojuegosDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para jugar?</li>
                <div class="ejemplo">Ejemplo: a partir de las 10 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="videojuegosHorarioFijo" id="videojuegosHorarioFijoSi" onclick="horario()" value="S">
                    <label for="videojuegosHorarioFijoSi">Si</label>

                    <input type="radio" name="videojuegosHorarioFijo" id="videojuegosHorarioFijoNo" onclick="horario()" value="N">
                    <label for="videojuegosHorarioFijoNo">No</label>
                </div>

                <div id="videojuegosHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suele empezar a jugar?</li>
                    <div class="ejemplo">Ejemplo: a las 10 de la noche.</div>

                    <div class="input-container">
                        <input type="number" name="videojuegos_horario" placeholder="10" min="1" max="12">
                        <select name="videojuegos_horario1" id="videojuegos_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="videojuegosHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele jugar?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 20hrs y las 22hrs.</div>

                    <div class="input-container">
                        <input type="number" name="videojuegos_horario100" placeholder="20" min="0" max="21">hrs
                        <input type="number" name="videojuegos_horario200" placeholder="22" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo acostumbra jugar?</li>
                <div class="ejemplo">Ejemplo: 2 horas.</div>

                <div class="input-container ">
                    <input type="number" name="videojuegos_duracionHrs" placeholder="2" min="0" max="24"> Horas
                    <input type="number" name="videojuegos_duracionMin" placeholder="0 " min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Redes sociales</h3>
        <br>
        <ol>
            <li>¿Usted dedica tiempo a las redes sociales?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="redesSocialesSioNo" id="redesSocialesSi" value="S" onclick="main()" required>
                <label for="redesSocialesSi">Si</label>

                <input type="radio" name="redesSocialesSioNo" id="redesSocialesNo" value="N" onclick="main()" required>
                <label for="redesSocialesNo">No</label>
            </div>

            <div id="noRedesSociales">
                <li>¿Tiene días determinados para entrar a las redes sociales?</li>
                <div class="ejemplo">Ejemplo: todos los días.</div>

                <div class="radio">
                    <input type="radio" name="redesSocialesDiaFijo" id="redesSocialesDiaFijoSi" value="S">
                    <label for="redesSocialesDiaFijoSi">Si</label>

                    <input type="radio" name="redesSocialesDiaFijo" id="redesSocialesDiaFijoNo" value="N">
                    <label for="redesSocialesDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para realizar esta actividad?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 6 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="redesSocialesHorarioFijo" id="redesSocialesHorarioFijoSi" onclick="horario()" value="S">
                    <label for="redesSocialesHorarioFijoSi">Si</label>

                    <input type="radio" name="redesSocialesHorarioFijo" id="redesSocialesHorarioFijoNo" onclick="horario()" value="N">
                    <label for="redesSocialesHorarioFijoNo">No</label>
                </div>


                <div id="redesSocialesHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: a las 6 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="redesSociales_horario" placeholder="6" min="1" max="12">
                        <select name="redesSociales_horario1" id="redesSociales_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="redesSocialesHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre 14hrs y 22hrs.</div>

                    <div class="input-container">
                        <input type="number" name="redesSociales_horario100" placeholder="14" min="0" max="21">hrs
                        <input type="number" name="redesSociales_horario200" placeholder="22" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="redesSociales_duracionHrs" placeholder="0" min="0" max="24"> Horas
                    <input type="number" name="redesSociales_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <div class="divBtn">
            <input type="submit" class="btn" value="Siguiente" name="btnEntretenimiento" />
        </div>
    </form>
</div>

<script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>