<!-- ***************************************************-->
<!-- *************************************************** -->
<!-- ********* Contenedor Formulario Estudiar ********** -->
<!-- *************************************************** -->
<!-- *************************************************** -->

<div id="containerEstudiar" class="containerBase">

    <form class="formulario" action="" method="post">

        <h1>Encuesta OTI:E</h1>

        <h2>IV. Estudiar</h2>

        <br>
        <h3>Tomar clases (de manera virtual)</h3>
        <br>
        <ol>
            <li>¿Usted toma clases de manera virtual?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="clasesVirtualesSioNo" id="clasesVirtualesSi" value="S" onclick="main()" required>
                <label for="clasesVirtualesSi">Si</label>

                <input type="radio" name="clasesVirtualesSioNo" id="clasesVirtualesNo" value="N" onclick="main()" required>
                <label for="clasesVirtualesNo">No</label>
            </div>

            <div id="noClasesVirtuales">
                <li>¿Tiene días determinados para tomar clases?</li>
                <div class="ejemplo">Ejemplo: de lunes a viernes.</div>

                <div class="radio">
                    <input type="radio" name="clasesVirtualesDiaFijo" id="clasesVirtualesDiaFijoSi" value="S">
                    <label for="clasesVirtualesDiaFijoSi">Si</label>

                    <input type="radio" name="clasesVirtualesDiaFijo" id="clasesVirtualesDiaFijoNo" value="N">
                    <label for="clasesVirtualesDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para tomar clases?</li>
                <div class="ejemplo">Ejemplo: de 8 de la mañana a 2 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="clasesVirtualesHorarioFijo" id="clasesVirtualesHorarioFijoSi" onclick="horario()" value="S">
                    <label for="clasesVirtualesHorarioFijoSi">Si</label>

                    <input type="radio" name="clasesVirtualesHorarioFijo" id="clasesVirtualesHorarioFijoNo" onclick="horario()" value="N">
                    <label for="clasesVirtualesHorarioFijoNo">No</label>
                </div>

                <div id="clasesVirtualesHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suelen empezar sus clases?</li>
                    <div class="ejemplo">Ejemplo: a las 8 de la mañana.</div>

                    <div class="input-container">
                        <input type="number" name="clasesVirtuales_horario" placeholder="8" min="1" max="12">
                        <select name="clasesVirtuales_horario1" id="clasesVirtuales_horarioAmPm">

                            <option value="am">AM</option>
                            <option value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="clasesVirtualesHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele tener sus clases?</li>
                    <div class="ejemplo">Nota1: en caso de tener clases "salteadas", considere desde la primera clase hasta la última clase del día.</div>
                    <div class="ejemplo">Nota2: en caso de tener diferentes horarios por dia, considere el de mayor horas de clases.</div>
                    <div class="ejemplo">Nota3: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 8hrs y 16hrs.</div>

                    <div class="input-container">
                        <input type="number" name="clasesVirtuales_horario100" placeholder="8" min="0" max="21">hrs
                        <input type="number" name="clasesVirtuales_horario200" placeholder="16" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo duran sus clases?</li>
                <div class="ejemplo">Nota1: en caso de tener clases "salteadas", considere unicamente la sumatoria de las horas de clases.</div>
                <div class="ejemplo">Nota2: en caso de tener diferentes horarios por dia, considere el de mayor horas de clases.</div>
                <div class="ejemplo">Ejemplo: 6 horas.</div>

                <div class="input-container ">
                    <input type="number" name="clasesVirtuales_duracionHrs" placeholder="6" min="0" max="16"> Horas
                    <input type="number" name="clasesVirtuales_duracionMin" placeholder="0" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <br>
        <h3>Tomar clases (de manera presenciales)</h3>
        <br>
        <ol>
            <li>¿Usted toma clases de manera presencial?</li>
            <!-- <div class="ejemplo"></div> -->

            <div class="radio">
                <input type="radio" name="clasesPresencialesSioNo" id="clasesPresencialesSi" value="S" onclick="main()" required>
                <label for="clasesPresencialesSi">Si</label>

                <input type="radio" name="clasesPresencialesSioNo" id="clasesPresencialesNo" value="N" onclick="main()" required>
                <label for="clasesPresencialesNo">No</label>
            </div>

            <div id="noClasesPresenciales">
                <li>¿Tiene días determinados para tomar clases?</li>
                <div class="ejemplo">Ejemplo: de lunes a viernes.</div>

                <div class="radio">
                    <input type="radio" name="clasesPresencialesDiaFijo" id="clasesPresencialesDiaFijoSi" value="S">
                    <label for="clasesPresencialesDiaFijoSi">Si</label>

                    <input type="radio" name="clasesPresencialesDiaFijo" id="clasesPresencialesDiaFijoNo" value="N">
                    <label for="clasesPresencialesDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para tomar clases?</li>
                <div class="ejemplo">Ejemplo: de 8 de la mañana a 2 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="clasesPresencialesHorarioFijo" id="clasesPresencialesHorarioFijoSi" onclick="horario()" value="S">
                    <label for="clasesPresencialesHorarioFijoSi">Si</label>

                    <input type="radio" name="clasesPresencialesHorarioFijo" id="clasesPresencialesHorarioFijoNo" onclick="horario()" value="N">
                    <label for="clasesPresencialesHorarioFijoNo">No</label>
                </div>


                <div id="clasesPresencialesHorarioFijo_si" style="display: none;">
                    <li>¿A que hora suelen empezar sus clases?</li>
                    <div class="ejemplo">Ejemplo: a las 8 de la mañana.</div>

                    <div class="input-container">
                        <input type="number" name="clasesPresenciales_horario" placeholder="8" min="1" max="12">
                        <select name="clasesPresenciales_horario1" id="clasesPresenciales_horarioAmPm">

                            <option value="am">AM</option>
                            <option value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="clasesPresencialesHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele tener sus clases?</li>
                    <div class="ejemplo">Nota1: en caso de tener clases "salteadas", considere desde la primera clase hasta la última clase del día.</div>
                    <div class="ejemplo">Nota2: en caso de tener diferentes horarios por dia, considere el de mayor horas de clases.</div>
                    <div class="ejemplo">Nota3: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 8hrs y las 16hrs.</div>

                    <div class="input-container">
                        <input type="number" name="clasesPresenciales_horario100" placeholder="8" min="0" max="21">hrs
                        <input type="number" name="clasesPresenciales_horario200" placeholder="16" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Cuánto tiempo duran sus clases?</li>
                <div class="ejemplo">Nota1: en caso de tener clases "salteadas", considere unicamente la sumatoria de las horas de clases.</div>
                <div class="ejemplo">Nota2: en caso de tener diferentes horarios por dia, considere el de mayor horas de clases.</div>
                <div class="ejemplo">Ejemplo: 6 horas.</div>

                <div class="input-container ">
                    <input type="number" name="clasesPresenciales_duracionHrs" placeholder="6" min="0" max="16"> Horas
                    <input type="number" name="clasesPresenciales_duracionMin" placeholder="0 " min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <div id="noClasesPresenciales2">
            <br>
            <h3>Transporte entre la casa y la escuela</h3>
            <br>
            <ol>

                <li>¿Tiene un horario determinado para transportarse?</li>
                <div class="ejemplo">Ejemplo: salgo a las 7 am de mi casa porque a las 7:15 am pasa el autobús.</div>

                <div class="radio">
                    <input type="radio" name="transporteCasaEscuelaHorarioFijo" id="transporteCasaEscuelaHorarioFijoSi" value="S">
                    <label for="transporteCasaEscuelaHorarioFijoSi">Si</label>

                    <input type="radio" name="transporteCasaEscuelaHorarioFijo" id="transporteCasaEscuelaHorarioFijoNo" value="N">
                    <label for="transporteCasaEscuelaHorarioFijoNo">No</label>
                </div>

                <li>¿Cúal es el horario en el que suele realizar esta actividad con mayour frecuencia? (independientemente de ser horario fijo o no)</li>
                <div class="ejemplo">Nota: considere unicamente el horario de transporte de su hogar a la escuela.</div>
                <div class="ejemplo">Ejemplo: si sale de su casa entre 7 y 8 de la mañana, coloque "7".</div>

                <div class="input-container">
                    <input type="number" name="transporteCasaEscuela_horario" placeholder="7" min="1" max="12">
                    <select name="transporteCasaEscuela_horario1" id="transporteCasaEscuela_horarioAmPm">

                        <option value="am">AM</option>
                        <option value="pm">PM</option>
                    </select>
                </div>

                <li>¿Aproximadamente en cuánto tiempo llega a su destino?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="transporteCasaEscuela_duracionHrs" placeholder="0" min="0" max="20"> Horas
                    <input type="number" name="transporteCasaEscuela_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>

            </ol>
        </div>

        <br>
        <h3>Estudiar (fuera del horario de clases)</h3>
        <br>
        <ol>
            <li>¿Usted dedica tiempo a estudiar fuera del horario de clases?</li>
            <div class="ejemplo">Nota: No aplican tareas, reportes, investigaciones, etc.</div>

            <div class="radio">
                <input type="radio" name="estudioAutodidactaSioNo" id="estudioAutodidactaSi" value="S" onclick="main()" required>
                <label for="estudioAutodidactaSi">Si</label>

                <input type="radio" name="estudioAutodidactaSioNo" id="estudioAutodidactaNo" value="N" onclick="main()" required>
                <label for="estudioAutodidactaNo">No</label>
            </div>

            <div id="noEstudioAutodidacta">
                <li>¿Tiene días determinados para estudiar?</li>
                <div class="ejemplo">Ejemplo: los lunes, jueves y sábados.</div>

                <div class="radio">
                    <input type="radio" name="estudioAutodidactaDiaFijo" id="estudioAutodidactaDiaFijoSi" value="S">
                    <label for="estudioAutodidactaDiaFijoSi">Si</label>

                    <input type="radio" name="estudioAutodidactaDiaFijo" id="estudioAutodidactaDiaFijoNo" value="N">
                    <label for="estudioAutodidactaDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para estudiar?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="estudioAutodidactaHorarioFijo" id="estudioAutodidactaHorarioFijoSi" onclick="horario()" value="S">
                    <label for="estudioAutodidactaHorarioFijoSi">Si</label>

                    <input type="radio" name="estudioAutodidactaHorarioFijo" id="estudioAutodidactaHorarioFijoNo" onclick="horario()" value="N">
                    <label for="estudioAutodidactaHorarioFijoNo">No</label>
                </div>


                <div id="estudioAutodidactaHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: empiezo a estudiar a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="estudioAutodidacta_horario" placeholder="4" min="1" max="12">
                        <select name="estudioAutodidacta_horario1" id="estudioAutodidacta_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="estudioAutodidactaHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 14hrs y las 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="estudioAutodidacta_horario100" placeholder="14" min="0" max="21">hrs
                        <input type="number" name="estudioAutodidacta_horario200" placeholder="18" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="estudioAutodidacta_duracionHrs" placeholder="0" min="0" max="20"> Horas
                    <input type="number" name="estudioAutodidacta_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>

        <br>
        <h3>Realizar tareas escolares</h3>
        <br>
        <ol>

            <div id="noHaceTarea">
                <li>¿Tiene días determinados para hacer tarea?</li>
                <div class="ejemplo">Ejemplo: de lunes a sábado.</div>

                <div class="radio">
                    <input type="radio" name="hacerTareaDiaFijo" id="hacerTareaDiaFijoSi" value="S" required>
                    <label for="hacerTareaDiaFijoSi">Si</label>

                    <input type="radio" name="hacerTareaDiaFijo" id="hacerTareaDiaFijoNo" value="N" required>
                    <label for="hacerTareaDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para hacer la tarea?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 10 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="hacerTareaHorarioFijo" id="hacerTareaHorarioFijoSi" onclick="horario()" value="S" required>
                    <label for="hacerTareaHorarioFijoSi">Si</label>

                    <input type="radio" name="hacerTareaHorarioFijo" id="hacerTareaHorarioFijoNo" onclick="horario()" value="N" required>
                    <label for="hacerTareaHorarioFijoNo">No</label>
                </div>


                <div id="hacerTareaHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: empiezo a hacer la tarea a las 10 de la noche.</div>

                    <div class="input-container">
                        <input type="number" name="hacerTarea_horario" placeholder="10" min="1" max="12">
                        <select name="hacerTarea_horario1" id="hacerTarea_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="hacerTareaHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 21hrs y 23hrs.</div>

                    <div class="input-container">
                        <input type="number" name="hacerTarea_horario100" placeholder="21" min="0" max="21">hrs
                        <input type="number" name="hacerTarea_horario200" placeholder="23" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="hacerTarea_duracionHrs" placeholder="0" min="0" max="24" required> Horas
                    <input type="number" name="hacerTarea_duracionMin" placeholder="30" min="0" max="59" required> Minutos
                </div>
            </div>
        </ol>


        <br>
        <h3>Realizar reportes escolares</h3>
        <br>
        <ol>

            <div id="noHaceReportes">
                <li>¿Tiene días determinados para realizar reportes?</li>
                <div class="ejemplo">Ejemplo: de lunes a sábado.</div>

                <div class="radio">
                    <input type="radio" name="hacerReportesDiaFijo" id="hacerReportesDiaFijoSi" value="S" required>
                    <label for="hacerReportesDiaFijoSi">Si</label>

                    <input type="radio" name="hacerReportesDiaFijo" id="hacerReportesDiaFijoNo" value="N" required>
                    <label for="hacerReportesDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para hacer reportes?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 10 de la noche.</div>

                <div class="radio">
                    <input type="radio" name="hacerReportesHorarioFijo" id="hacerReportesHorarioFijoSi" onclick="horario()" value="S" required>
                    <label for="hacerReportesHorarioFijoSi">Si</label>

                    <input type="radio" name="hacerReportesHorarioFijo" id="hacerReportesHorarioFijoNo" onclick="horario()" value="N" required>
                    <label for="hacerReportesHorarioFijoNo">No</label>
                </div>


                <div id="hacerReportesHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: empiezo a hacer los reportes a las 10 de la noche.</div>

                    <div class="input-container">
                        <input type="number" name="hacerReportes_horario" placeholder="10" min="1" max="12">
                        <select name="hacerReportes_horario1" id="hacerReportes_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="hacerReportesHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 20hrs y 23hrs.</div>

                    <div class="input-container">
                        <input type="number" name="hacerReportes_horario100" placeholder="20" min="0" max="21">hrs
                        <input type="number" name="hacerReportes_horario200" placeholder="23" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="hacerReportes_duracionHrs" placeholder="0" min="0" max="24" required> Horas
                    <input type="number" name="hacerReportes_duracionMin" placeholder="30" min="0" max="59" required> Minutos
                </div>
            </div>
        </ol>



        <br>
        <h3>Avance de proyecto/tesis/tesina</h3>
        <br>
        <ol>
            <li>¿Usted realiza un proyecto, tesis o tesina?</li>
            <div class="ejemplo">Ejemplo: proyecto tutorado, proyecto integrador, proyecto final, proyecto de una materia.</div>

            <div class="radio">
                <input type="radio" name="avanceProyectoSioNo" id="avanceProyectoSi" value="S" onclick="main()" required>
                <label for="avanceProyectoSi">Si</label>

                <input type="radio" name="avanceProyectoSioNo" id="avanceProyectoNo" value="N" onclick="main()" required>
                <label for="avanceProyectoNo">No</label>
            </div>

            <div id="noAvanceProyecto">
                <li>¿Tiene días determinados para realizar un avance?</li>
                <div class="ejemplo">Ejemplo: los lunes, jueves y sábados.</div>

                <div class="radio">
                    <input type="radio" name="avanceProyectoDiaFijo" id="avanceProyectoDiaFijoSi" value="S">
                    <label for="avanceProyectoDiaFijoSi">Si</label>

                    <input type="radio" name="avanceProyectoDiaFijo" id="avanceProyectoDiaFijoNo" value="N">
                    <label for="avanceProyectoDiaFijoNo">No</label>
                </div>

                <li>¿Tiene un horario fijo para realizar un avance?</li>
                <div class="ejemplo">Ejemplo: alrededor de las 4 de la tarde.</div>

                <div class="radio">
                    <input type="radio" name="avanceProyectoHorarioFijo" id="avanceProyectoHorarioFijoSi" onclick="horario()" value="S">
                    <label for="avanceProyectoHorarioFijoSi">Si</label>

                    <input type="radio" name="avanceProyectoHorarioFijo" id="avanceProyectoHorarioFijoNo" onclick="horario()" value="N">
                    <label for="avanceProyectoHorarioFijoNo">No</label>
                </div>

                <div id="avanceProyectoHorarioFijo_si" style="display: none;">
                    <li>¿Cúal es el horario en el que suele realizar esta actividad?</li>
                    <div class="ejemplo">Ejemplo: empiezo a realizar el avance a las 4 de la tarde.</div>

                    <div class="input-container">
                        <input type="number" name="avanceProyecto_horario" placeholder="4" min="1" max="12">
                        <select name="avanceProyecto_horario1" id="avanceProyecto_horarioAmPm">

                            <option value="am">AM</option>
                            <option selected value="pm">PM</option>
                        </select>
                    </div>
                </div>

                <div id="avanceProyectoHorarioFijo_no" style="display: none;">
                    <li>¿Entre qué horas suele realizar esta actividad?</li>
                    <div class="ejemplo">Nota: formato de 24hrs.</div>
                    <div class="ejemplo">Ejemplo: entre las 16hrs y 18hrs.</div>

                    <div class="input-container">
                        <input type="number" name="avanceProyecto_horario100" placeholder="16" min="0" max="21">hrs
                        <input type="number" name="avanceProyecto_horario200" placeholder="18" min="2" max="23">hrs

                    </div>
                </div>


                <li>¿Aproximadamente cuánto tiempo dedica a esta actividad?</li>
                <!-- <div class="ejemplo">Ejemplo: si desayuna en 30 minutos, coloque "30".</div> -->

                <div class="input-container ">
                    <input type="number" name="avanceProyecto_duracionHrs" placeholder="0" min="0" max="24"> Horas
                    <input type="number" name="avanceProyecto_duracionMin" placeholder="30" min="0" max="59"> Minutos
                </div>
            </div>
        </ol>


        <div class="divBtn">
            <input type="submit" class="btn" value="Siguiente" name="btnEstudiar" />
        </div>
    </form>
</div>
<script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>