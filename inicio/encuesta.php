<?php
// include("./conexion.php");
include("../conexion.php");

session_start();

$nombre = $_SESSION['nombre'];
$id_user = $_SESSION['id'];
$encuestaRealizada = $_SESSION['encuesta'];


if ($nombre == '' || $nombre == null) {
    echo "<script>
    alert('¡Atención! sesión no iniciada, se redireccionará para su inicio de sesión.');
    window.location = '../login.php';
    </script>";
    die();
}
 if($encuestaRealizada == 'S'){
     echo"<script>
     alert('Ya ha respondido esta encuesta.');
     window.location = '../inicio.php';
     </scritp>";
     die();
 }

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../CSS/encuesta.css">

    <title>Encuesta OTI:E</title>
</head>

<body>


    <div id="containerx">
        <header id="headerx">
            <div id="menu">
                <ul>
                    <li id="regresar">
                        <a href="../inicio.php">
                            Regresar
                        </a>
                    </li>

                    <li id="perfil">
                        <a href="#">
                            Perfil
                        </a>
                    </li>

                    <li id="cerrarSesion">
                        <a href="../CerrarSesion.php">
                            Cerrar sesión
                        </a>
                    </li>

                </ul>
            </div>
        </header>
    </div>


    <?php
    include "./encuesta-forms/formAlimentacion.php";
    
    include "./encuesta-forms/formLimpieza.php";

    include "./encuesta-forms/formHigiene.php";
    
    include "./encuesta-forms/formEstudio.php";

    include "./encuesta-forms/formEntretenimiento.php";

    include "./encuesta-forms/formVidaSocial.php";

    include "./encuesta-forms/formOtrasActividades.php";

    include "./encuesta-process.php";

    ?>

    <footer> Autor: Juan Arias Castillo ITSM - MSC - Tecnologías de programación</footer>


    <script src="encuesta.js"></script>
    <script src="encuestaHorarios.js"></script>

</body>

</html>