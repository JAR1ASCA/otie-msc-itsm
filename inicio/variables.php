<?php
/**********---Variables ---**********/
/**********- Alimentación -**********/
//desayuno
$desayuno_horarioFijo = $_REQUEST["desayunoHorarioFijo"];
$desayuno_horario = $_POST["desayuno_horario"];

$desayuno_horario1 = $_POST["desayuno_horario1"];
$desayuno_horario2 = $_POST["desayuno_horario2"];

$desayuno_duracionHrs = $_POST["desayuno_duracionHrs"];
$desayuno_duracionMin = $_POST["desayuno_duracionMin"];
$desayuno_duracion = 0;

//comida
$comida_horarioFijo = $_REQUEST["comidaHorarioFijo"];
$comida_horario = $_POST["comida_horario"];

$comida_horario1 = $_POST["comida_horario1"];
$comida_horario2 = $_POST["comida_horario2"];

$comida_duracionHrs = $_POST["comida_duracionHrs"];
$comida_duracionMin = $_POST["comida_duracionMin"];
$comida_duracion = 0;

//cena
$cena_horarioFijo = $_REQUEST["cenaHorarioFijo"];
$cena_horario = $_POST["cena_horario"];

$cena_horario1 = $_POST["cena_horario1"];
$cena_horario2 = $_POST["cena_horario2"];

$cena_duracionHrs = $_POST["cena_duracionHrs"];
$cena_duracionMin = $_POST["cena_duracionMin"];
$cena_duracion = 0;

//preparar comida
$prepararC_SioNo = $_REQUEST["prepararComidaSioNo"];
$prepararC_diaFijo = $_REQUEST["prepararComidaDiafijo"];
$prepararC_horarioFijo = $_REQUEST["prepararComidaHorariofijo"];

$prepararC_tipoComida1 = $_REQUEST["prepararComidaHorario11"];
$prepararC_horario1 = $_POST["prepararComidaHorario1"];
$prepararC_horarioAmPm1 = $_REQUEST["prepararComidaTipo1"];
// $prepararC_horario1 = 0;
// $prepararC_tipoComida1 = "Ni";


$prepararC_tipoComida2 = $_REQUEST["prepararComidaHorario21"];
$prepararC_horario2 = $_POST["prepararComidaHorario2"];
$prepararC_horarioAmPm2 = $_REQUEST["prepararComidaTipo2"];
// $prepararC_horario2 = 0;
// $prepararC_tipoComida2 = "Ni";

$prepararC_tipoComida3 = $_REQUEST["prepararComidaHorario31"];
$prepararC_horario3 = $_POST["prepararComidaHorario3"];
$prepararC_horarioAmPm3 = $_REQUEST["prepararComidaTipo3"];
// $prepararC_horario3 = 0;
// $prepararC_tipoComida3 = "Ni";

$prepararC_duracionHrs1 = $_POST["prepararComida_duracionHrs1"];
$prepararC_duracionMin1 = $_POST["prepararComida_duracionMin1"];
$prepararC_duracion1 = 0;

$prepararC_duracionHrs2 = $_POST["prepararComida_duracionHrs2"];
$prepararC_duracionMin2 = $_POST["prepararComida_duracionMin2"];
$prepararC_duracion2 = 0;

$prepararC_duracionHrs3 = $_POST["prepararComida_duracionHrs3"];
$prepararC_duracionMin3 = $_POST["prepararComida_duracionMin3"];
$prepararC_duracion3 = 0;



/**********- Limpieza -**********/
//lavar trastes
$lavarTrastes_SioNo = $_REQUEST["lavarTrastesSioNo"];
$lavarTrastes_diaFijo = $_REQUEST["lavarTrastesDiaFijo"];
$lavarTrastes_horarioFijo = $_REQUEST["lavarTrastesHorarioFijo"];

$lavarTrastes_horario = $_POST["lavarTrastes_horario"];
$lavarTrastes_horarioAmPm = $_REQUEST["lavarTrastes_horario1"];

$lavarTrastes_horario1 = $_POST["lavarTrastes_horario100"];
$lavarTrastes_horario2 = $_POST["lavarTrastes_horario200"];

$lavarTrastes_duracionHrs = $_POST["lavarTrastes_duracionHrs"];
$lavarTrastes_duracionMin = $_POST["lavarTrastes_duracionMin"];
$lavarTrastes_duracion = 0;

//barrer
$barrer_SioNo = $_REQUEST["barrerSioNo"];
$barrer_diaFijo = $_REQUEST["barrerDiaFijo"];
$barrer_horarioFijo = $_REQUEST["barrerHorarioFijo"];

$barrer_horario = $_POST["barrer_horario"];
$barrer_horarioAmPm = $_REQUEST["barrer_horario1"];

$barrer_horario1 = $_POST["barrer_horario100"];
$barrer_horario2 = $_POST["barrer_horario200"];

$barrer_duracionHrs = $_POST["barrer_duracionHrs"];
$barrer_duracionMin = $_POST["barrer_duracionMin"];
$barrer_duracion = 0;

//trapear
$trapear_SioNo = $_REQUEST["trapearSioNo"];
$trapear_diaFijo = $_REQUEST["trapearDiaFijo"];
$trapear_horarioFijo = $_REQUEST["trapearHorarioFijo"];

$trapear_horario = $_POST["trapear_horario"];
$trapear_horarioAmPm = $_REQUEST["trapear_horario1"];

$trapear_horario1 = $_POST["trapear_horario100"];
$trapear_horario2 = $_POST["trapear_horario200"];

$trapear_duracionHrs = $_POST["trapear_duracionHrs"];
$trapear_duracionMin = $_POST["trapear_duracionMin"];
$trapear_duracion = 0;

//ordenar cuarto
// $ordenarCuarto_SioNo = $_REQUEST["ordenarCuartoSioNo"]; //no necesario
$ordenarCuarto_diaFijo = $_REQUEST["ordenarCuartoDiaFijo"];
$ordenarCuarto_horarioFijo = $_REQUEST["ordenarCuartoHorarioFijo"];

$ordenarCuarto_horario = $_POST["ordenarCuarto_horario"];
$ordenarCuarto_horarioAmPm = $_REQUEST["ordenarCuarto_horario1"];

$ordenarCuarto_horario1 = $_POST["ordenarCuarto_horario100"];
$ordenarCuarto_horario2 = $_POST["ordenarCuarto_horario200"];

$ordenarCuarto_duracionHrs = $_POST["ordenarCuarto_duracionHrs"];
$ordenarCuarto_duracionMin = $_POST["ordenarCuarto_duracionMin"];
$ordenarCuarto_duracion = 0;

//lavar ropa
$lavarRopa_SioNo = $_REQUEST["lavarRopaSioNo"];
$lavarRopa_diaFijo = $_REQUEST["lavarRopaDiaFijo"];
$lavarRopa_horarioFijo = $_REQUEST["lavarRopaHorarioFijo"];

$lavarRopa_horario = $_POST["lavarRopa_horario"];
$lavarRopa_horarioAmPm = $_REQUEST["lavarRopa_horario1"];

$lavarRopa_horario1 = $_POST["lavarRopa_horario100"];
$lavarRopa_horario2 = $_POST["lavarRopa_horario200"];

$lavarRopa_duracionHrs = $_POST["lavarRopa_duracionHrs"];
$lavarRopa_duracionMin = $_POST["lavarRopa_duracionMin"];
$lavarRopa_duracion = 0;


/**********- Higiene personal -**********/
//bañarse
// $lavarRopa_SioNo = $_REQUEST["lavarRopaSioNo"]; //no necesario
$bañarse_diaFijo = $_REQUEST["bañarseDiaFijo"];
$bañarse_horarioFijo = $_REQUEST["bañarseHorarioFijo"];

$bañarse_horario = $_POST["bañarse_horario"];
$bañarse_horarioAmPm = $_REQUEST["bañarse_horario1"];

$bañarse_horario1 = $_POST["bañarse_horario100"];
$bañarse_horario2 = $_POST["bañarse_horario200"];

$bañarse_duracionHrs = $_POST["bañarse_duracionHrs"];
$bañarse_duracionMin = $_POST["bañarse_duracionMin"];
$bañarse_duracion = 0;

//maquillarse
$maquillarse_SioNo = $_REQUEST["maquillarseSioNo"];
$maquillarse_diaFijo = $_REQUEST["maquillarseDiaFijo"];
$maquillarse_horarioFijo = $_REQUEST["maquillarseHorarioFijo"];

$maquillarse_horario = $_POST["maquillarse_horario"];
$maquillarse_horarioAmPm = $_REQUEST["maquillarse_horario1"];

$maquillarse_horario1 = $_POST["maquillarse_horario100"];
$maquillarse_horario2 = $_POST["maquillarse_horario200"];

$maquillarse_duracionHrs = $_POST["maquillarse_duracionHrs"];
$maquillarse_duracionMin = $_POST["maquillarse_duracionMin"];
$maquillarse_duracion = 0;

//desmaquillarse
// $desmaquillarse_SioNo = $_REQUEST["desmaquillarseSioNo"];//no necesario 
$desmaquillarse_diaFijo = $_REQUEST["desmaquillarseDiaFijo"];
$desmaquillarse_horarioFijo = $_REQUEST["desmaquillarseHorarioFijo"];

$desmaquillarse_horario = $_POST["desmaquillarse_horario"];
$desmaquillarse_horarioAmPm = $_REQUEST["desmaquillarse_horario1"];

$desmaquillarse_horario1 = $_POST["desmaquillarse_horario100"];
$desmaquillarse_horario2 = $_POST["desmaquillarse_horario200"];

$desmaquillarse_duracionHrs = $_POST["desmaquillarse_duracionHrs"];
$desmaquillarse_duracionMin = $_POST["desmaquillarse_duracionMin"];
$desmaquillarse_duracion = 0;


/**********- Estudio -**********/
//clases virtuales
$clasesVirtuales_SioNo = $_REQUEST["clasesVirtualesSioNo"];
$clasesVirtuales_diaFijo = $_REQUEST["clasesVirtualesDiaFijo"];
$clasesVirtuales_horarioFijo = $_REQUEST["clasesVirtualesHorarioFijo"];

$clasesVirtuales_horario = $_POST["clasesVirtuales_horario"];
$clasesVirtuales_horarioAmPm = $_REQUEST["clasesVirtuales_horario1"];

$clasesVirtuales_horario1 = $_POST["clasesVirtuales_horario100"];
$clasesVirtuales_horario2 = $_POST["clasesVirtuales_horario200"];

$clasesVirtuales_duracionHrs = $_POST["clasesVirtuales_duracionHrs"];
$clasesVirtuales_duracionMin = $_POST["clasesVirtuales_duracionMin"];
$clasesVirtuales_duracion = 0;

//clases presenciales
$clasesPresenciales_SioNo = $_REQUEST["clasesPresencialesSioNo"];
$clasesPresenciales_diaFijo = $_REQUEST["clasesPresencialesDiaFijo"];
$clasesPresenciales_horarioFijo = $_REQUEST["clasesPresencialesHorarioFijo"];

$clasesPresenciales_horario = $_POST["clasesPresenciales_horario"];
$clasesPresenciales_horarioAmPm = $_REQUEST["clasesPresenciales_horario1"];

$clasesPresenciales_horario1 = $_POST["clasesPresenciales_horario100"];
$clasesPresenciales_horario2 = $_POST["clasesPresenciales_horario200"];

$clasesPresenciales_duracionHrs = $_POST["clasesPresenciales_duracionHrs"];
$clasesPresenciales_duracionMin = $_POST["clasesPresenciales_duracionMin"];
$clasesPresenciales_duracion = 0;

//transporte casa-escuela y viceversa
// $transporteCE_SioNo = $_REQUEST["transporteCESioNo"];    //no necesario
// $transporteCE_diaFijo = $_REQUEST["transporteCEDiaFijo"];    //no necesario
$transporteCE_horarioFijo = $_REQUEST["transporteCasaEscuelaHorarioFijo"];

$transporteCE_horario = $_POST["transporteCasaEscuela_horario"];
$transporteCE_horarioAmPm = $_REQUEST["transporteCasaEscuela_horario1"];

$transporteCE_horario1 = $_POST["transporteCasaEscuela_horario100"];
$transporteCE_horario2 = $_POST["transporteCasaEscuela_horario200"];

$transporteCE_duracionHrs = $_POST["transporteCasaEscuela_duracionHrs"];
$transporteCE_duracionMin = $_POST["transporteCasaEscuela_duracionMin"];
$transporteCE_duracion = 0;

//estudio autodidacta
$estudioAuto_SioNo = $_REQUEST["estudioAutodidactaSioNo"];
$estudioAuto_diaFijo = $_REQUEST["estudioAutodidactaDiaFijo"];
$estudioAuto_horarioFijo = $_REQUEST["estudioAutodidactaHorarioFijo"];

$estudioAuto_horario = $_POST["estudioAutodidacta_horario"];
$estudioAuto_horarioAmPm = $_REQUEST["estudioAutodidacta_horario1"];

$estudioAuto_horario1 = $_POST["estudioAutodidacta_horario100"];
$estudioAuto_horario2 = $_POST["estudioAutodidacta_horario200"];

$estudioAuto_duracionHrs = $_POST["estudioAutodidacta_duracionHrs"];
$estudioAuto_duracionMin = $_POST["estudioAutodidacta_duracionMin"];
$estudioAuto_duracion = 0;

//hacer tarea
// $hacerTarea_SioNo = $_REQUEST["hacerTareaSioNo"];    //no necesario
$hacerTarea_diaFijo = $_REQUEST["hacerTareaDiaFijo"];
$hacerTarea_horarioFijo = $_REQUEST["hacerTareaHorarioFijo"];

$hacerTarea_horario = $_POST["hacerTarea_horario"];
$hacerTarea_horarioAmPm = $_REQUEST["hacerTarea_horario1"];

$hacerTarea_horario1 = $_POST["hacerTarea_horario100"];
$hacerTarea_horario2 = $_POST["hacerTarea_horario200"];

$hacerTarea_duracionHrs = $_POST["hacerTarea_duracionHrs"];
$hacerTarea_duracionMin = $_POST["hacerTarea_duracionMin"];
$hacerTarea_duracion = 0;

//hacer reportes
// $hacerReportes_SioNo = $_REQUEST["hacerReportesSioNo"];    //no necesario
$hacerReportes_diaFijo = $_REQUEST["hacerReportesDiaFijo"];
$hacerReportes_horarioFijo = $_REQUEST["hacerReportesHorarioFijo"];

$hacerReportes_horario = $_POST["hacerReportes_horario"];
$hacerReportes_horarioAmPm = $_REQUEST["hacerReportes_horario1"];

$hacerReportes_horario1 = $_POST["hacerReportes_horario100"];
$hacerReportes_horario2 = $_POST["hacerReportes_horario200"];

$hacerReportes_duracionHrs = $_POST["hacerReportes_duracionHrs"];
$hacerReportes_duracionMin = $_POST["hacerReportes_duracionMin"];
$hacerReportes_duracion = 0;

//avance proyecto/tesis
$avanceProyecto_SioNo = $_REQUEST["avanceProyectoSioNo"];
$avanceProyecto_diaFijo = $_REQUEST["avanceProyectoDiaFijo"];
$avanceProyecto_horarioFijo = $_REQUEST["avanceProyectoHorarioFijo"];

$avanceProyecto_horario = $_POST["avanceProyecto_horario"];
$avanceProyecto_horarioAmPm = $_REQUEST["avanceProyecto_horario1"];

$avanceProyecto_horario1 = $_POST["avanceProyecto_horario100"];
$avanceProyecto_horario2 = $_POST["avanceProyecto_horario200"];

$avanceProyecto_duracionHrs = $_POST["avanceProyecto_duracionHrs"];
$avanceProyecto_duracionMin = $_POST["avanceProyecto_duracionMin"];
$avanceProyecto_duracion = 0;



/**********- Entretenimiento -**********/
//ver tv o series
$verTvSerie_SioNo = $_REQUEST["verTvSerieSioNo"];
$verTvSerie_diaFijo = $_REQUEST["verTvSerieDiaFijo"];
$verTvSerie_horarioFijo = $_REQUEST["verTvSerieHorarioFijo"];

$verTvSerie_horario = $_POST["verTvSerie_horario"];
$verTvSerie_horarioAmPm = $_REQUEST["verTvSerie_horario1"];

$verTvSerie_horario1 = $_POST["verTvSerie_horario100"];
$verTvSerie_horario2 = $_POST["verTvSerie_horario200"];

$verTvSerie_duracionHrs = $_POST["verTvSerie_duracionHrs"];
$verTvSerie_duracionMin = $_POST["verTvSerie_duracionMin"];
$verTvSerie_duracion = 0;

//videojuegos
$videojuegos_SioNo = $_REQUEST["videojuegosSioNo"];
$videojuegos_diaFijo = $_REQUEST["videojuegosDiaFijo"];
$videojuegos_horarioFijo = $_REQUEST["videojuegosHorarioFijo"];

$videojuegos_horario = $_POST["videojuegos_horario"];
$videojuegos_horarioAmPm = $_REQUEST["videojuegos_horario1"];

$videojuegos_horario1 = $_POST["videojuegos_horario100"];
$videojuegos_horario2 = $_POST["videojuegos_horario200"];

$videojuegos_duracionHrs = $_POST["videojuegos_duracionHrs"];
$videojuegos_duracionMin = $_POST["videojuegos_duracionMin"];
$videojuegos_duracion = 0;

//RedesSociales
$redesSociales_SioNo = $_REQUEST["redesSocialesSioNo"];
$redesSociales_diaFijo = $_REQUEST["redesSocialesDiaFijo"];
$redesSociales_horarioFijo = $_REQUEST["redesSocialesHorarioFijo"];

$redesSociales_horario = $_POST["redesSociales_horario"];
$redesSociales_horarioAmPm = $_REQUEST["redesSociales_horario1"];

$redesSociales_horario1 = $_POST["redesSociales_horario100"];
$redesSociales_horario2 = $_POST["redesSociales_horario200"];

$redesSociales_duracionHrs = $_POST["redesSociales_duracionHrs"];
$redesSociales_duracionMin = $_POST["redesSociales_duracionMin"];
$redesSociales_duracion = 0;


/**********- Vida social -**********/
//salir con amigos
$salirConAmigos_SioNo = $_REQUEST["salirConAmigosSioNo"];
$salirConAmigos_diaFijo = $_REQUEST["salirConAmigosDiaFijo"];
$salirConAmigos_horarioFijo = $_REQUEST["salirConAmigosHorarioFijo"];

$salirConAmigos_horario = $_POST["salirConAmigos_horario"];
$salirConAmigos_horarioAmPm = $_REQUEST["salirConAmigos_horario1"];

$salirConAmigos_horario1 = $_POST["salirConAmigos_horario100"];
$salirConAmigos_horario2 = $_POST["salirConAmigos_horario200"];

$salirConAmigos_duracionHrs = $_POST["salirConAmigos_duracionHrs"];
$salirConAmigos_duracionMin = $_POST["salirConAmigos_duracionMin"];
$salirConAmigos_duracion = 0;

//salir con la novia o novio
$salirConNovio_SioNo = $_REQUEST["salirConNovioSioNo"];
$salirConNovio_diaFijo = $_REQUEST["salirConNovioDiaFijo"];
$salirConNovio_horarioFijo = $_REQUEST["salirConNovioHorarioFijo"];

$salirConNovio_horario = $_POST["salirConNovio_horario"];
$salirConNovio_horarioAmPm = $_REQUEST["salirConNovio_horario1"];

$salirConNovio_horario1 = $_POST["salirConNovio_horario100"];
$salirConNovio_horario2 = $_POST["salirConNovio_horario200"];

$salirConNovio_duracionHrs = $_POST["salirConNovio_duracionHrs"];
$salirConNovio_duracionMin = $_POST["salirConNovio_duracionMin"];
$salirConNovio_duracion = 0;

//evento social
$eventoSocial_SioNo = $_REQUEST["eventoSocialSioNo"];
$eventoSocial_diaFijo = $_REQUEST["eventoSocialDiaFijo"];
$eventoSocial_horarioFijo = $_REQUEST["eventoSocialHorarioFijo"];

$eventoSocial_horario = $_POST["eventoSocial_horario"];
$eventoSocial_horarioAmPm = $_REQUEST["eventoSocial_horario1"];

$eventoSocial_horario1 = $_POST["eventoSocial_horario100"];
$eventoSocial_horario2 = $_POST["eventoSocial_horario200"];

$eventoSocial_duracionHrs = $_POST["eventoSocial_duracionHrs"];
$eventoSocial_duracionMin = $_POST["eventoSocial_duracionMin"];
$eventoSocial_duracion = 0;



/**********- Otras actividades -**********/
//hacer ejercicio
$hacerEjercicio_SioNo = $_REQUEST["HacerEjercicioSioNo"];
$hacerEjercicio_diaFijo = $_REQUEST["HacerEjercicioDiaFijo"];
$hacerEjercicio_horarioFijo = $_REQUEST["HacerEjercicioHorarioFijo"];

$hacerEjercicio_horario = $_POST["HacerEjercicio_horario"];
$hacerEjercicio_horarioAmPm = $_REQUEST["HacerEjercicio_horario1"];

$hacerEjercicio_horario1 = $_POST["HacerEjercicio_horario100"];
$hacerEjercicio_horario2 = $_POST["HacerEjercicio_horario200"];

$hacerEjercicio_duracionHrs = $_POST["HacerEjercicio_duracionHrs"];
$hacerEjercicio_duracionMin = $_POST["HacerEjercicio_duracionMin"];
$hacerEjercicio_duracion = 0;

//trabajar
$trabajar_SioNo = $_REQUEST["trabajarSioNo"];
$trabajar_diaFijo = $_REQUEST["trabajarDiaFijo"];
$trabajar_horarioFijo = $_REQUEST["trabajarHorarioFijo"];

$trabajar_horario = $_POST["trabajar_horario"];
$trabajar_horarioAmPm = $_REQUEST["trabajar_horario1"];

$trabajar_horario1 = $_POST["trabajar_horario100"];
$trabajar_horario2 = $_POST["trabajar_horario200"];

$trabajar_duracionHrs = $_POST["trabajar_duracionHrs"];
$trabajar_duracionMin = $_POST["trabajar_duracionMin"];
$trabajar_duracion = 0;

//hacer compras
$hacerCompras_SioNo = $_REQUEST["hacerComprasSioNo"];
$hacerCompras_diaFijo = $_REQUEST["hacerComprasDiaFijo"];
$hacerCompras_horarioFijo = $_REQUEST["hacerComprasHorarioFijo"];

$hacerCompras_horario = $_POST["hacerCompras_horario"];
$hacerCompras_horarioAmPm = $_REQUEST["hacerCompras_horario1"];

$hacerCompras_horario1 = $_POST["hacerCompras_horario100"];
$hacerCompras_horario2 = $_POST["hacerCompras_horario200"];

$hacerCompras_duracionHrs = $_POST["hacerCompras_duracionHrs"];
$hacerCompras_duracionMin = $_POST["hacerCompras_duracionMin"];
$hacerCompras_duracion = 0;

//pasear perro
$pasearPerro_SioNo = $_REQUEST["pasearPerroSioNo"];
$pasearPerro_diaFijo = $_REQUEST["pasearPerroDiaFijo"];
$pasearPerro_horarioFijo = $_REQUEST["pasearPerroHorarioFijo"];

$pasearPerro_horario = $_POST["pasearPerro_horario"];
$pasearPerro_horarioAmPm = $_REQUEST["pasearPerro_horario1"];

$pasearPerro_horario1 = $_POST["pasearPerro_horario100"];
$pasearPerro_horario2 = $_POST["pasearPerro_horario200"];

$pasearPerro_duracionHrs = $_POST["pasearPerro_duracionHrs"];
$pasearPerro_duracionMin = $_POST["pasearPerro_duracionMin"];
$pasearPerro_duracion = 0;

//bañar perro
$bañarPerro_SioNo = $_REQUEST["bañarPerroSioNo"];
$bañarPerro_diaFijo = $_REQUEST["bañarPerroDiaFijo"];
$bañarPerro_horarioFijo = $_REQUEST["bañarPerroHorarioFijo"];

$bañarPerro_horario = $_POST["bañarPerro_horario"];
$bañarPerro_horarioAmPm = $_REQUEST["bañarPerro_horario1"];

$bañarPerro_horario1 = $_POST["bañarPerro_horario100"];
$bañarPerro_horario2 = $_POST["bañarPerro_horario200"];

$bañarPerro_duracionHrs = $_POST["bañarPerro_duracionHrs"];
$bañarPerro_duracionMin = $_POST["bañarPerro_duracionMin"];
$bañarPerro_duracion = 0;

//dormir
// $bañarPerro_SioNo = $_REQUEST["bañarPerroSioNo"];  
// $bañarPerro_diaFijo = $_REQUEST["bañarPerroDiaFijo"];
$dormir_horarioFijo = $_REQUEST["dormirHorarioFijo"];

$dormir_horario = $_POST["dormir_horario"];
$dormir_horarioAmPm = $_REQUEST["dormir_horario1"];

$dormir_horario1 = $_POST["dormir_horario100"];
$dormir_horario2 = $_POST["dormir_horario200"];

$dormir_duracionHrs = $_POST["dormir_duracionHrs"];
$dormir_duracionMin = $_POST["dormir_duracionMin"];
$dormir_duracion = 0;

?>