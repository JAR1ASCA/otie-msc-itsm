function horario() {
    //desayuno
    const desayunoFijoSi = document.getElementById("desayunoSi").checked;
    const contenedorDesayunoHorarioFijoSi = document.getElementById("desayunoHorarioFijo_si");
    const contenedorDesayunoHorarioFijoNo = document.getElementById("desayunoHorarioFijo_no");

    //comida
    const comidaFijoSi = document.getElementById("comidaSi").checked;
    const contenedorComidaHorarioFijoSi = document.getElementById("comidaHorarioFijo_si");
    const contenedorComidaHorarioFijoNo = document.getElementById("comidaHorarioFijo_no");

    //cena
    const cenaFijoSi = document.getElementById("cenaSi").checked;
    const contenedorcenaHorarioFijoSi = document.getElementById("cenaHorarioFijo_si");
    const contenedorcenaHorarioFijoNo = document.getElementById("cenaHorarioFijo_no");


    //lavar trastes
    const lavarTrastesHorarioFijoSi = document.getElementById("lavarTrastesHorarioFijoSi").checked;
    const contenedorlavarTrastesHorarioFijoSi = document.getElementById("lavarTrastesHorarioFijo_si");
    const contenedorlavarTrastesHorarioFijoNo = document.getElementById("lavarTrastesHorarioFijo_no");

    //barrer
    const barrerHorarioFijoSi = document.getElementById("barrerHorarioFijoSi").checked;
    const contenedorbarrerHorarioFijoSi = document.getElementById("barrerHorarioFijo_si");
    const contenedorbarrerHorarioFijoNo = document.getElementById("barrerHorarioFijo_no");

    //trapear
    const trapearHorarioFijoSi = document.getElementById("trapearHorarioFijoSi").checked;
    const contenedortrapearHorarioFijoSi = document.getElementById("trapearHorarioFijo_si");
    const contenedortrapearHorarioFijoNo = document.getElementById("trapearHorarioFijo_no");

    //ordenarCuarto
    const ordenarCuartoHorarioFijoSi = document.getElementById("ordenarCuartoHorarioFijoSi").checked;
    const contenedorordenarCuartoHorarioFijoSi = document.getElementById("ordenarCuartoHorarioFijo_si");
    const contenedorordenarCuartoHorarioFijoNo = document.getElementById("ordenarCuartoHorarioFijo_no");


    //lavarRopa
    const lavarRopaHorarioFijoSi = document.getElementById("lavarRopaHorarioFijoSi").checked;
    const contenedorlavarRopaHorarioFijoSi = document.getElementById("lavarRopaHorarioFijo_si");
    const contenedorlavarRopaHorarioFijoNo = document.getElementById("lavarRopaHorarioFijo_no");



    //bañarse
    const bañarseHorarioFijoSi = document.getElementById("bañarseHorarioFijoSi").checked;
    const contenedorbañarseHorarioFijoSi = document.getElementById("bañarseHorarioFijo_si");
    const contenedorbañarseHorarioFijoNo = document.getElementById("bañarseHorarioFijo_no");

    //maquillarse
    const maquillarseHorarioFijoSi = document.getElementById("maquillarseHorarioFijoSi").checked;
    const contenedormaquillarseHorarioFijoSi = document.getElementById("maquillarseHorarioFijo_si");
    const contenedormaquillarseHorarioFijoNo = document.getElementById("maquillarseHorarioFijo_no");

    //desmaquillarse
    const desmaquillarseHorarioFijoSi = document.getElementById("desmaquillarseHorarioFijoSi").checked;
    const contenedordesmaquillarseHorarioFijoSi = document.getElementById("desmaquillarseHorarioFijo_si");
    const contenedordesmaquillarseHorarioFijoNo = document.getElementById("desmaquillarseHorarioFijo_no");


    //clasesVirtuales
    const clasesVirtualesHorarioFijoSi = document.getElementById("clasesVirtualesHorarioFijoSi").checked;
    const contenedorclasesVirtualesHorarioFijoSi = document.getElementById("clasesVirtualesHorarioFijo_si");
    const contenedorclasesVirtualesHorarioFijoNo = document.getElementById("clasesVirtualesHorarioFijo_no");

    //clasesPresenciales
    const clasesPresencialesHorarioFijoSi = document.getElementById("clasesPresencialesHorarioFijoSi").checked;
    const contenedorclasesPresencialesHorarioFijoSi = document.getElementById("clasesPresencialesHorarioFijo_si");
    const contenedorclasesPresencialesHorarioFijoNo = document.getElementById("clasesPresencialesHorarioFijo_no");

    //estudioAutodidacta
    const estudioAutodidactaHorarioFijoSi = document.getElementById("estudioAutodidactaHorarioFijoSi").checked;
    const contenedorestudioAutodidactaHorarioFijoSi = document.getElementById("estudioAutodidactaHorarioFijo_si");
    const contenedorestudioAutodidactaHorarioFijoNo = document.getElementById("estudioAutodidactaHorarioFijo_no");

    //hacerTarea
    const hacerTareaHorarioFijoSi = document.getElementById("hacerTareaHorarioFijoSi").checked;
    const contenedorhacerTareaHorarioFijoSi = document.getElementById("hacerTareaHorarioFijo_si");
    const contenedorhacerTareaHorarioFijoNo = document.getElementById("hacerTareaHorarioFijo_no");

    //hacerReportes
    const hacerReportesHorarioFijoSi = document.getElementById("hacerReportesHorarioFijoSi").checked;
    const contenedorhacerReportesHorarioFijoSi = document.getElementById("hacerReportesHorarioFijo_si");
    const contenedorhacerReportesHorarioFijoNo = document.getElementById("hacerReportesHorarioFijo_no");

    //avanceProyecto
    const avanceProyectoHorarioFijoSi = document.getElementById("avanceProyectoHorarioFijoSi").checked;
    const contenedoravanceProyectoHorarioFijoSi = document.getElementById("avanceProyectoHorarioFijo_si");
    const contenedoravanceProyectoHorarioFijoNo = document.getElementById("avanceProyectoHorarioFijo_no");

    //verTvSerie
    const verTvSerieHorarioFijoSi = document.getElementById("verTvSerieHorarioFijoSi").checked;
    const contenedorverTvSerieHorarioFijoSi = document.getElementById("verTvSerieHorarioFijo_si");
    const contenedorverTvSerieHorarioFijoNo = document.getElementById("verTvSerieHorarioFijo_no");

    //videojuegos
    const videojuegosHorarioFijoSi = document.getElementById("videojuegosHorarioFijoSi").checked;
    const contenedorvideojuegosHorarioFijoSi = document.getElementById("videojuegosHorarioFijo_si");
    const contenedorvideojuegosHorarioFijoNo = document.getElementById("videojuegosHorarioFijo_no");

    //redesSociales
    const redesSocialesHorarioFijoSi = document.getElementById("redesSocialesHorarioFijoSi").checked;
    const contenedorredesSocialesHorarioFijoSi = document.getElementById("redesSocialesHorarioFijo_si");
    const contenedorredesSocialesHorarioFijoNo = document.getElementById("redesSocialesHorarioFijo_no");

    //salirConAmigos
    const salirConAmigosHorarioFijoSi = document.getElementById("salirConAmigosHorarioFijoSi").checked;
    const contenedorsalirConAmigosHorarioFijoSi = document.getElementById("salirConAmigosHorarioFijo_si");
    const contenedorsalirConAmigosHorarioFijoNo = document.getElementById("salirConAmigosHorarioFijo_no");

    //salirConNovio
    const salirConNovioHorarioFijoSi = document.getElementById("salirConNovioHorarioFijoSi").checked;
    const contenedorsalirConNovioHorarioFijoSi = document.getElementById("salirConNovioHorarioFijo_si");
    const contenedorsalirConNovioHorarioFijoNo = document.getElementById("salirConNovioHorarioFijo_no");

    //eventoSocial
    const eventoSocialHorarioFijoSi = document.getElementById("eventoSocialHorarioFijoSi").checked;
    const contenedoreventoSocialHorarioFijoSi = document.getElementById("eventoSocialHorarioFijo_si");
    const contenedoreventoSocialHorarioFijoNo = document.getElementById("eventoSocialHorarioFijo_no");

    //HacerEjercicio
    const HacerEjercicioHorarioFijoSi = document.getElementById("HacerEjercicioHorarioFijoSi").checked;
    const contenedorHacerEjercicioHorarioFijoSi = document.getElementById("HacerEjercicioHorarioFijo_si");
    const contenedorHacerEjercicioHorarioFijoNo = document.getElementById("HacerEjercicioHorarioFijo_no");

    //trabajarHorario
    const trabajarHorarioFijoSi = document.getElementById("trabajarHorarioFijoSi").checked;
    const contenedortrabajarHorarioFijoSi = document.getElementById("trabajarHorarioFijo_si");
    const contenedortrabajarHorarioFijoNo = document.getElementById("trabajarHorarioFijo_no");

    //hacerCompras
    const hacerComprasHorarioFijoSi = document.getElementById("hacerComprasHorarioFijoSi").checked;
    const contenedorhacerComprasHorarioFijoSi = document.getElementById("hacerComprasHorarioFijo_si");
    const contenedorhacerComprasHorarioFijoNo = document.getElementById("hacerComprasHorarioFijo_no");

    //pasearPerro
    const pasearPerroHorarioFijoSi = document.getElementById("pasearPerroHorarioFijoSi").checked;
    const contenedorpasearPerroHorarioFijoSi = document.getElementById("pasearPerroHorarioFijo_si");
    const contenedorpasearPerroHorarioFijoNo = document.getElementById("pasearPerroHorarioFijo_no");

    //bañarPerro
    const bañarPerroHorarioFijoSi = document.getElementById("bañarPerroHorarioFijoSi").checked;
    const contenedorbañarPerroHorarioFijoSi = document.getElementById("bañarPerroHorarioFijo_si");
    const contenedorbañarPerroHorarioFijoNo = document.getElementById("bañarPerroHorarioFijo_no");

    //dormir
    const dormirHorarioFijoSi = document.getElementById("dormirHorarioFijoSi").checked;
    const contenedordormirHorarioFijoSi = document.getElementById("dormirHorarioFijo_si");
    const contenedordormirHorarioFijoNo = document.getElementById("dormirHorarioFijo_no");






    if (desayunoFijoSi) { //desayuno fijo
        contenedorDesayunoHorarioFijoSi.style.display = "block";
        contenedorDesayunoHorarioFijoNo.style.display = "none";
    } else {
        contenedorDesayunoHorarioFijoSi.style.display = "none";
        contenedorDesayunoHorarioFijoNo.style.display = "block";
    }

    if (comidaFijoSi) { //comida fijo
        contenedorComidaHorarioFijoSi.style.display = "block";
        contenedorComidaHorarioFijoNo.style.display = "none";
    } else {
        contenedorComidaHorarioFijoSi.style.display = "none";
        contenedorComidaHorarioFijoNo.style.display = "block";
    }

    if (cenaFijoSi) { //cena fijo
        contenedorcenaHorarioFijoSi.style.display = "block";
        contenedorcenaHorarioFijoNo.style.display = "none";
    } else {
        contenedorcenaHorarioFijoSi.style.display = "none";
        contenedorcenaHorarioFijoNo.style.display = "block";
    }

    if (lavarTrastesHorarioFijoSi) { //lavarTrastes fijo
        contenedorlavarTrastesHorarioFijoSi.style.display = "block";
        contenedorlavarTrastesHorarioFijoNo.style.display = "none";
    } else {
        contenedorlavarTrastesHorarioFijoSi.style.display = "none";
        contenedorlavarTrastesHorarioFijoNo.style.display = "block";
    }

    if (barrerHorarioFijoSi) { //barrer fijo
        contenedorbarrerHorarioFijoSi.style.display = "block";
        contenedorbarrerHorarioFijoNo.style.display = "none";
    } else {
        contenedorbarrerHorarioFijoSi.style.display = "none";
        contenedorbarrerHorarioFijoNo.style.display = "block";
    }

    if (trapearHorarioFijoSi) { //trapear fijo
        contenedortrapearHorarioFijoSi.style.display = "block";
        contenedortrapearHorarioFijoNo.style.display = "none";
    } else {
        contenedortrapearHorarioFijoSi.style.display = "none";
        contenedortrapearHorarioFijoNo.style.display = "block";
    }

    if (ordenarCuartoHorarioFijoSi) { //ordenarCuarto fijo
        contenedorordenarCuartoHorarioFijoSi.style.display = "block";
        contenedorordenarCuartoHorarioFijoNo.style.display = "none";
    } else {
        contenedorordenarCuartoHorarioFijoSi.style.display = "none";
        contenedorordenarCuartoHorarioFijoNo.style.display = "block";
    }

    if (lavarRopaHorarioFijoSi) { //lavarRopa fijo
        contenedorlavarRopaHorarioFijoSi.style.display = "block";
        contenedorlavarRopaHorarioFijoNo.style.display = "none";
    } else {
        contenedorlavarRopaHorarioFijoSi.style.display = "none";
        contenedorlavarRopaHorarioFijoNo.style.display = "block";
    }

    if (bañarseHorarioFijoSi) { //bañarse fijo
        contenedorbañarseHorarioFijoSi.style.display = "block";
        contenedorbañarseHorarioFijoNo.style.display = "none";
    } else {
        contenedorbañarseHorarioFijoSi.style.display = "none";
        contenedorbañarseHorarioFijoNo.style.display = "block";
    }

    if (maquillarseHorarioFijoSi) { //maquillarse fijo
        contenedormaquillarseHorarioFijoSi.style.display = "block";
        contenedormaquillarseHorarioFijoNo.style.display = "none";
    } else {
        contenedormaquillarseHorarioFijoSi.style.display = "none";
        contenedormaquillarseHorarioFijoNo.style.display = "block";
    }

    if (desmaquillarseHorarioFijoSi) { //desmaquillarse fijo
        contenedordesmaquillarseHorarioFijoSi.style.display = "block";
        contenedordesmaquillarseHorarioFijoNo.style.display = "none";
    } else {
        contenedordesmaquillarseHorarioFijoSi.style.display = "none";
        contenedordesmaquillarseHorarioFijoNo.style.display = "block";
    }

    if (clasesVirtualesHorarioFijoSi) { //clasesVirtuales fijo
        contenedorclasesVirtualesHorarioFijoSi.style.display = "block";
        contenedorclasesVirtualesHorarioFijoNo.style.display = "none";
    } else {
        contenedorclasesVirtualesHorarioFijoSi.style.display = "none";
        contenedorclasesVirtualesHorarioFijoNo.style.display = "block";
    }

    if (clasesPresencialesHorarioFijoSi) { //clasesPresenciales fijo
        contenedorclasesPresencialesHorarioFijoSi.style.display = "block";
        contenedorclasesPresencialesHorarioFijoNo.style.display = "none";
    } else {
        contenedorclasesPresencialesHorarioFijoSi.style.display = "none";
        contenedorclasesPresencialesHorarioFijoNo.style.display = "block";
    }

    if (estudioAutodidactaHorarioFijoSi) { //estudioAutodidacta fijo
        contenedorestudioAutodidactaHorarioFijoSi.style.display = "block";
        contenedorestudioAutodidactaHorarioFijoNo.style.display = "none";
    } else {
        contenedorestudioAutodidactaHorarioFijoSi.style.display = "none";
        contenedorestudioAutodidactaHorarioFijoNo.style.display = "block";
    }

    if (hacerTareaHorarioFijoSi) { //hacerTarea fijo
        contenedorhacerTareaHorarioFijoSi.style.display = "block";
        contenedorhacerTareaHorarioFijoNo.style.display = "none";
    } else {
        contenedorhacerTareaHorarioFijoSi.style.display = "none";
        contenedorhacerTareaHorarioFijoNo.style.display = "block";
    }

    if (hacerReportesHorarioFijoSi) { //hacerReportes fijo
        contenedorhacerReportesHorarioFijoSi.style.display = "block";
        contenedorhacerReportesHorarioFijoNo.style.display = "none";
    } else {
        contenedorhacerReportesHorarioFijoSi.style.display = "none";
        contenedorhacerReportesHorarioFijoNo.style.display = "block";
    }

    if (avanceProyectoHorarioFijoSi) { //avanceProyecto fijo
        contenedoravanceProyectoHorarioFijoSi.style.display = "block";
        contenedoravanceProyectoHorarioFijoNo.style.display = "none";
    } else {
        contenedoravanceProyectoHorarioFijoSi.style.display = "none";
        contenedoravanceProyectoHorarioFijoNo.style.display = "block";
    }

    if (verTvSerieHorarioFijoSi) { //verTvSerie fijo
        contenedorverTvSerieHorarioFijoSi.style.display = "block";
        contenedorverTvSerieHorarioFijoNo.style.display = "none";
    } else {
        contenedorverTvSerieHorarioFijoSi.style.display = "none";
        contenedorverTvSerieHorarioFijoNo.style.display = "block";
    }

    if (videojuegosHorarioFijoSi) { //videojuegos fijo
        contenedorvideojuegosHorarioFijoSi.style.display = "block";
        contenedorvideojuegosHorarioFijoNo.style.display = "none";
    } else {
        contenedorvideojuegosHorarioFijoSi.style.display = "none";
        contenedorvideojuegosHorarioFijoNo.style.display = "block";
    }

    if (redesSocialesHorarioFijoSi) { //redesSociales fijo
        contenedorredesSocialesHorarioFijoSi.style.display = "block";
        contenedorredesSocialesHorarioFijoNo.style.display = "none";
    } else {
        contenedorredesSocialesHorarioFijoSi.style.display = "none";
        contenedorredesSocialesHorarioFijoNo.style.display = "block";
    }

    if (salirConAmigosHorarioFijoSi) { //salirConAmigos fijo
        contenedorsalirConAmigosHorarioFijoSi.style.display = "block";
        contenedorsalirConAmigosHorarioFijoNo.style.display = "none";
    } else {
        contenedorsalirConAmigosHorarioFijoSi.style.display = "none";
        contenedorsalirConAmigosHorarioFijoNo.style.display = "block";
    }

    if (salirConNovioHorarioFijoSi) { //salirConNovio fijo
        contenedorsalirConNovioHorarioFijoSi.style.display = "block";
        contenedorsalirConNovioHorarioFijoNo.style.display = "none";
    } else {
        contenedorsalirConNovioHorarioFijoSi.style.display = "none";
        contenedorsalirConNovioHorarioFijoNo.style.display = "block";
    }

    if (eventoSocialHorarioFijoSi) { //eventoSocial fijo
        contenedoreventoSocialHorarioFijoSi.style.display = "block";
        contenedoreventoSocialHorarioFijoNo.style.display = "none";
    } else {
        contenedoreventoSocialHorarioFijoSi.style.display = "none";
        contenedoreventoSocialHorarioFijoNo.style.display = "block";
    }


    if (HacerEjercicioHorarioFijoSi) { //HacerEjercicio fijo
        contenedorHacerEjercicioHorarioFijoSi.style.display = "block";
        contenedorHacerEjercicioHorarioFijoNo.style.display = "none";
    } else {
        contenedorHacerEjercicioHorarioFijoSi.style.display = "none";
        contenedorHacerEjercicioHorarioFijoNo.style.display = "block";
    }
    if (trabajarHorarioFijoSi) { //trabajarHorario fijo
        contenedortrabajarHorarioFijoSi.style.display = "block";
        contenedortrabajarHorarioFijoNo.style.display = "none";
    } else {
        contenedortrabajarHorarioFijoSi.style.display = "none";
        contenedortrabajarHorarioFijoNo.style.display = "block";
    }
    if (hacerComprasHorarioFijoSi) { //hacerComprasHorario fijo
        contenedorhacerComprasHorarioFijoSi.style.display = "block";
        contenedorhacerComprasHorarioFijoNo.style.display = "none";
    } else {
        contenedorhacerComprasHorarioFijoSi.style.display = "none";
        contenedorhacerComprasHorarioFijoNo.style.display = "block";
    }

    if (pasearPerroHorarioFijoSi) { //pasearPerroHorario fijo
        contenedorpasearPerroHorarioFijoSi.style.display = "block";
        contenedorpasearPerroHorarioFijoNo.style.display = "none";
    } else {
        contenedorpasearPerroHorarioFijoSi.style.display = "none";
        contenedorpasearPerroHorarioFijoNo.style.display = "block";
    }
    if (bañarPerroHorarioFijoSi) { //bañarPerroHorario fijo
        contenedorbañarPerroHorarioFijoSi.style.display = "block";
        contenedorbañarPerroHorarioFijoNo.style.display = "none";
    } else {
        contenedorbañarPerroHorarioFijoSi.style.display = "none";
        contenedorbañarPerroHorarioFijoNo.style.display = "block";
    }

    if (dormirHorarioFijoSi) { //dormirHorario fijo
        contenedordormirHorarioFijoSi.style.display = "block";
        contenedordormirHorarioFijoNo.style.display = "none";
    } else {
        contenedordormirHorarioFijoSi.style.display = "none";
        contenedordormirHorarioFijoNo.style.display = "block";
    }




}