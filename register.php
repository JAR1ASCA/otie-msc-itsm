<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/f5f0dc57eb.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="CSS/estiloLoginRegister.css">
    <title>Registrarse</title>
</head>

<body>

    <form class="formulario" action="" method="post">

        <h1>Registrarse</h1>
        <center>
            <div id="statusRegistro" style="
            background-color: rgb(3, 180, 3); height: 30px; width:95%; display:flex;
            justify-content: center; align-items: center; opacity:0;">

                Registro exitoso

            </div>
        </center>
        <div class="container">
            <div class="input-container">
                <i class="fas fa-user icon"></i>
                <input type="text" placeholder="Nombre Completo" name="nombre" required autofocus>
            </div>


            <div class="input-container">
                <i class="fas fa-user icon"></i>
                <input type="number" name="edad" placeholder="Edad" min="17" max="100" required style="font-size: 20px; border: none; padding: 5px; margin: 5px; width: 20%;">
                <!-- <label><input type="checkbox" id ="cbox1" value="Prefiero" >Prefiero no decirlo</label> -->
            </div>


            <div class="input-container">
                <i class="fas fa-venus-mars" style="color: gray; margin-left: 15px; margin-bottom: 10px;margin-top: 10px; position: absolute;"></i>

                <div class="radio">
                    <input type="radio" name="genero" id="hombre" value="H" required>
                    <label for="hombre">Hombre</label>

                    <input type="radio" name="genero" id="mujer" value="M" required>
                    <label for="mujer">Mujer</label>

                    <input type="radio" name="genero" id="otro" value="O" required>
                    <label for="otro">Otro</label>
                </div>
            </div>

            <div class="input-container nivel-academico">
                <i class="fas fa-graduation-cap icon"></i> Nivel académico

                <div class="radio">
                    <input type="radio" name="grado" id="licenciatura" value="L" required>
                    <label for="licenciatura">Licenciatura</label>

                    <input type="radio" name="grado" id="posgrado" value="P" required>
                    <label for="posgrado">Posgrado</label>
                </div>
            </div>

            <div class="input-container">
                <i class="fas fa-envelope icon"></i>
                <input type="email" placeholder="Correo electrónico" name="correo" required>
            </div>

            <div class="input-container">
                <i class="fas fa-key icon"></i>
                <input type="password" placeholder="Contraseña" name="contraseña" required>
            </div>

            <div>
                <input type="submit" class="boton" value="Registrarse" name="btnRegister">
            </div>

            <div id="respuesta" style="display: block;">
            </div>

            <p>Al registrarse, acepta nuestra política de privacidad y términos de servicio.</p>
            <p>
                ¿Ya tiene una cuenta?
                <a class="link" href="login.php"> Iniciar Sesión</a>
            </p>

        </div>

    </form>
    
    <?php
    include("conexion.php");

    //variables
    $nombre = $_POST["nombre"];
    $edad = $_POST["edad"];
    $contraseña = $_POST["contraseña"];
    $correo = $_POST["correo"];
    $status = 'A';
    $nivelEdu = $_REQUEST['grado'];
    $genero = $_REQUEST['genero'];

    //registro
    if (isset($_POST["btnRegister"])) {

        $pwd = password_hash($contraseña, PASSWORD_DEFAULT, ['cost' => 10]);

        $query2 = mysqli_query($conn, "SELECT * FROM usuario WHERE correo = '$correo'");
        $nb2 = mysqli_num_rows($query2);

        if ($nb2 == 1) {
            // echo "<script> alert('Ya existe un usuario registrado con el correo proporcionado');</script>";
            echo "<script> var x = document.getElementById('statusRegistro');
            x.style.opacity='1';
            x.style.backgroundColor = 'red';
            x.style.color = 'white';
            x.textContent = 'Registro fallido: El correo ya ha sido registrado.';
            </script>";
        } else {

            $sqlRegistrar = "INSERT INTO usuario(nombre,edad,contraseña,loginStatus,fechaRegistro,correo,nivelEdu,genero) VALUES ('$nombre','$edad','$pwd','$status',NOW(),'$correo','$nivelEdu','$genero')";

            if (mysqli_query($conn, $sqlRegistrar)) {
                // echo "<script> alert('Usuario registrado con éxito: $nombre'); </script>";
                echo "<script> var x = document.getElementById('statusRegistro');
                x.style.opacity='1';
                x.style.backgroundColor = 'rgb(3, 180, 3)';
                x.textContent = 'Registro exitoso';
                window.location= './login.php';
                </script>";
            } else {
                echo "<script> alert('Error'); </script>";
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        }
    }
    ?>



</body>

</html>