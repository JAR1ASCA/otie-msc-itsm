<?php
session_start();

$nombre = $_SESSION['nombre'];

if ($nombre == '' || $nombre == null) {
}else{
    echo "<script>
    window.location = './inicio.php';
    </script>";
}

?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Organizador de tareas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/estiloIndex.css">
    <script src="https://kit.fontawesome.com/f5f0dc57eb.js" crossorigin="anonymous"></script>
</head>

<body>


    <div id="containerx">
        <header id="headerx">
            <div id="menu">
                <ul>
                    <li id="logo">
                        <a href="#">
                            <img src="RES/img/1-logo.png" width="80" />
                        </a>
                    </li>

                    <li id="acerca">
                        <a href="javascript:abrir()">
                            Acerca de OTI:E
                        </a>
                    </li>

                    <li id="encuesta">
                        <a href="encuesta.php">
                            <!-- Encuesta -->
                        </a>
                    </li>

                    <li id="login">
                        <a href="login.php">
                            Login
                        </a>
                    </li>

                </ul>
            </div>
        </header>
    </div>

    <div id="container1">
        <main>
            <div class="text">
                <h1>Organizador de Tareas Inteligente: Estudiante</h1>

                <p id="p1">
                    La mejor app para organizar tus tareas de manera Inteligente
                </p>

                <p id="p2">
                    <a href="login.php">
                        <button> Empezar ahora</button>
                    </a>
                </p>
            </div>
        </main>
    </div>

    <footer> Autor: Juan Arias Castillo ITSM - MSC - Tecnologías de programación</footer>


    <script>
        function abrir() {
            document.getElementById("ventanaEmergente").style.display = "block";
            document.getElementById("acercaDe").style.display = "block";
        }

        function cerrar() {
            document.getElementById("ventanaEmergente").style.display = "none";
        }
    </script>


<div id="ventanaEmergente">
    <div id="acercaDe">
        <div id="cerrar">
            <a href="javascript:cerrar()">
                <img src="RES/icons/close.svg" width="20">
            </a>
        </div>

        <div>
            <h4>Acerca de...</h4>
            <p>OTI:E es una aplicación desarrollada para la materia Tecnologías de programación.</p>
            <p>Posgrado: Maestría en Sistemas Computacionales</p>
            <p>Escuela: Instituto Tecnológico Superior de Misantla</p>
            <p>Profesor: Dr. Eddy Sánchez de la Cruz</p>
            <p>Alumno: Juan Arias Castillo</p>
        </div>
    </div>
</div>


</body>

</html>